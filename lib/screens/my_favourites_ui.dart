import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/my_favourites_controller.dart';
import '../utils/localisations/app_colors.dart';
import '../utils/localisations/app_strings.dart';

class MyFavouritesUI extends StatelessWidget {
  const MyFavouritesUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyFavouritesController controller = Get.find();
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        elevation: 2,
        leading: InkButton(
          backGroundColor: AppColors.accent,
          child: Icon(
            Icons.arrow_back,
            color: AppColors.white,
          ),
          onTap: () {
            Get.back();
          },
        ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
        title: "My Favourites".text.size(17).color(AppColors.grey).make(),
        centerTitle: true,
        backgroundColor: AppColors.white,
      ),
      body: Column(
        children: [
          StatefulBuilder(builder:
              (BuildContext context, void Function(void Function()) setState) {
            return controller.favourite.isNotEmpty
                ? ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.favourite.length,
                    itemBuilder: (BuildContext context, int index) {
                      return favouriteWidget(
                          index: index,
                          setState: setState,
                          controller: controller);
                    })
                : Center(
                    child:
                        "No Items".text.size(20).color(AppColors.grey).make(),
                  ).pOnly(left: 20, top: 300);
          })
        ],
      ).pOnly(top: 20, left: 10),
    ));
  }
}

Widget favouriteWidget(
    {required int index,
    required StateSetter setState,
    required MyFavouritesController controller}) {
  return SizedBox(
    height: 160,
    width: Get.width * .9,
    child: Stack(
      children: [
        Container(
          alignment: Alignment.center,
          height: 102,
          width: Get.width * .7,
          decoration: BoxDecoration(
              color: AppColors.white,
              boxShadow: [
                BoxShadow(
                  color: AppColors.grey,
                  blurRadius: 1.0,
                ),
              ],
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  controller.favourite[index].itemsName.text.make(),
                  InkWell(
                    onTap: () {
                      setState(() {
                        controller.favourite
                            .remove(controller.favourite[index]);
                      });
                    },
                    child: Icon(
                      controller.favourite[index].isFav
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: AppColors.red,
                    ),
                  ),
                ],
              ).pOnly(left: 50, right: 15),
              Align(
                alignment: Alignment.topLeft,
                child:
                    ("${AppStrings.dollar}${controller.favourite[index].finalPrice}")
                        .text
                        .size(18)
                        .fontWeight(FontWeight.w700)
                        .color(AppColors.buttonColorBlue)
                        .make(),
              ).pOnly(left: 60),
              StatefulBuilder(
                builder: (BuildContext context,
                    void Function(void Function()) setState) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      VxRating(
                        size: 15,
                        normalColor: Colors.yellow.shade200,
                        selectionColor: AppColors.yellow,
                        isSelectable: false,
                        onRatingUpdate: (value) {
                          setState(() {});
                          value = controller.favourite[index].rating as String;
                        },
                        value: 2,
                      ),
                      controller.favourite[index].rating.text
                          .size(10)
                          .color(AppColors.grey)
                          .make(),
                      Container(
                        height: 34,
                        width: 61,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: AppColors.accent),
                        child: Icon(
                          Icons.shopping_cart_outlined,
                          color: AppColors.white,
                        ),
                      ).onTap(() {
                        Get.toNamed(AppRoutes.cart);
                      })
                    ],
                  );
                },
              ).pOnly(left: 50, right: 20),
            ],
          ),
        ).positioned(left: 70, top: 15),
        Container(
            alignment: Alignment.center,
            height: 132,
            width: 105,
            decoration: BoxDecoration(
                color: AppColors.backgroundColor,
                borderRadius: BorderRadius.circular(15),
              boxShadow: [
              BoxShadow(
              color: AppColors.grey,
              blurRadius: 1.0,
            ),]
            ),
            child: CachedNetworkImage(
              imageUrl: controller.favourite[index].itemsImages,
              placeholder: (context, url) => SizedBox(
                child: LinearProgressIndicator(
                  color: AppColors.accentRipple,
                  minHeight: 10,
                ),
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            )
            ),
      ],
    ),
  ).onTap(() {
    // Get.toNamed(AppRoutes.productPage,
    //     arguments: ProductDetails(
    //       controller.popularList[index].itemsName,
    //       controller.popularList[index].finalPrice ,
    //       controller.popularList[index].itemsImages,
    //     ));
  }).pOnly(left: 10);
}
