import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/cart_controller.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:velocity_x/velocity_x.dart';

class CartUI extends StatefulWidget {
  const CartUI({Key? key}) : super(key: key);

  @override
  State<CartUI> createState() => _CartUIState();
}

class _CartUIState extends State<CartUI> {
  @override
  Widget build(BuildContext context) {
    CartController controller = Get.find();
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar:
      AppBar(
        centerTitle: true,
        backgroundColor: AppColors.white,
        leading: InkButton(
            height: 5,
            width: 5,
            borderRadius: 30,
            backGroundColor: AppColors.accent,
            child: Icon(
              Icons.arrow_back,
              color: AppColors.white,
            ),
            onTap: () {
              Get.back();
            }).pOnly(left: 10, top: 10, bottom: 10, right: 10),
        title: "My Cart".text.size(14).color(AppColors.grey).make(),
        actions: [
          PopupMenuButton(
            iconSize: 25,
            itemBuilder: (BuildContext context) {
              return [
                const PopupMenuItem(
                  value: 0,
                  child: Text('Filter'),
                ),
                const PopupMenuItem(
                  value: 1,
                  child: Text('Clear Cart'),
                ),

              ];
            },
            onSelected: (value) {
             if(value==1){
               setState(() {
                 controller.allCartItems.clear();

               });
             }
            },
            offset: const Offset(0, 50), // adjust the position of the menu
          ),
        ],
      ),
      body: SlidingUpPanel(
        borderRadius: BorderRadius.circular(15),
        minHeight: 145,
        maxHeight: 145,
        panel: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                "SubTotal"
                    .text
                    .color(AppColors.grey)
                    .size(14)
                    .fontWeight(FontWeight.w600)
                    .make(),
                "\$${controller.totalPrice}"
                    .text
                    .color(AppColors.red)
                    .size(20)
                    .fontWeight(FontWeight.w600)
                    .make(),
              ],
            ),
            InkButton(
              onTap: () {
                if (controller.selectedCartItems.isNotEmpty) {
                  Get.toNamed(
                    AppRoutes.payments,
                  );
                } else {
                  snackBar(
                      title: 'OrderEase', message: 'Please select items first');
                }
              },
              height: 52,
              width: Get.width,
              backGroundColor: AppColors.buttonColorBlue,
              child: "CHECK OUT"
                  .text
                  .color(AppColors.white)
                  .size(14)
                  .fontWeight(FontWeight.w700)
                  .make(),
            ).py(20),
          ],
        ).pOnly(left: 30, right: 30, top: 20),
        body: SingleChildScrollView(
          child: Column(
            children: [
              controller.allCartItems.isNotEmpty
                  ? SizedBox(
                      height: 558.0,
                      child: ListView.builder(
                          itemCount: controller.allCartItems.length,
                          shrinkWrap: true,
                          itemBuilder: (_, int index) {
                            return cartItems(
                                index: index,
                                controller: controller,
                                model: controller.allCartItems[index]);
                          }),
                    )
                  : Center(
                      child: "Cart is empty"
                          .text
                          .size(20)
                          .color(AppColors.grey)
                          .make(),
                    ),
            ],
          ).pOnly(top: 15),
        ),
      ),
    ));
  }

  Widget cartItems(
      {required int index,
      required CartController controller,
      required CartModel model}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 18.0),
      child: Container(
        height: 167,
        width: Get.width,
        color: AppColors.white,
        child:
        Slidable(
            enabled: true,
            closeOnScroll: true,
            startActionPane: ActionPane(
              motion: const ScrollMotion(),
              children: [
                SlidableAction(
                  autoClose: true,
                  flex: 1,
                  onPressed: ((context) {
                    setState(() {
                      controller.cart(Types.addItems, model, index, setState);
                    });
                  }),
                  backgroundColor: controller.selectedCartItems.contains(index)
                      ? AppColors.accentRipple
                      : AppColors.white,
                  foregroundColor: Colors.black,
                  icon: controller.selectedCartItems.contains(index)
                      ? Icons.check_circle_outline_sharp
                      : Icons.circle_outlined,
                )
              ],
            ),
            endActionPane: ActionPane(
              motion: const ScrollMotion(),
              children: [
                SlidableAction(
                  autoClose: true,
                  flex: 1,
                  onPressed: ((context) {
                    controller.cart(
                        Types.deleteItemsFromCart, model, index, setState);
                  }),
                  backgroundColor: AppColors.red,
                  foregroundColor: Colors.black,
                  icon: CupertinoIcons.delete,
                )
              ],
            ),
            child: Container(
              color: controller.selectedCartItems.contains(index)
                  ? AppColors.accentRipple
                  : AppColors.white,
              child: Row(
                children: [
                  Container(
                    height: 124,
                    width: Get.width * .35,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        imageUrl: model.itemsImage,
                        placeholder: (context, url) => SizedBox(
                          child: LinearProgressIndicator(
                            color: AppColors.accentRipple,
                            minHeight: 10,
                          ),
                        ),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                      ),
                    ),
                  ).pOnly(left: 10, right: 10, bottom: 30),
                  Column(
                    children: [
                      Container(
                        height: 124,
                        width: Get.width * .59,
                        color: controller.selectedCartItems.contains(index)
                            ? AppColors.accentRipple
                            : AppColors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            model.itemsName.text
                                .size(14)
                                .fontWeight(FontWeight.w500)
                                .color(
                                    controller.selectedCartItems.contains(index)
                                        ? AppColors.white
                                        : AppColors.grey)
                                .make(),
                            "Colour:${model.itemsColor}"
                                .text
                                .fontWeight(FontWeight.w400)
                                .size(10)
                                .color(
                                    controller.selectedCartItems.contains(index)
                                        ? AppColors.white
                                        : AppColors.grey)
                                .make(),
                            "Size:${model.size}"
                                .text
                                .size(10)
                                .fontWeight(FontWeight.w400)
                                .color(
                                    controller.selectedCartItems.contains(index)
                                        ? AppColors.white
                                        : AppColors.grey)
                                .make(),
                            "\$${model.price}"
                                .text
                                .size(18)
                                .fontWeight(FontWeight.w400)
                                .color(
                                    controller.selectedCartItems.contains(index)
                                        ? AppColors.white
                                        : AppColors.grey)
                                .make(),
                            Container(
                              alignment: Alignment.center,
                              height: 23,
                              width: Get.width * .39,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    height: 23,
                                    width: Get.width * .1,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: AppColors.grey),
                                    ),
                                    child: InkButton(
                                        height: 23,
                                        rippleColor: AppColors.grey,
                                        width: Get.width * .1,
                                        borderRadius: 0,
                                        backGroundColor: AppColors.white,
                                        child: "-".text.size(20).make(),
                                        onTap: () {
                                          controller.cart(Types.decrement,
                                              model, index, setState);
                                        }),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    height: 23,
                                    width: Get.width * .19,
                                    decoration: BoxDecoration(
                                      color: AppColors.white,
                                      border: Border.all(color: AppColors.grey),
                                    ),
                                    child: model.numberOfCartItem.text.make(),
                                  ),
                                  Container(
                                    alignment: Alignment.topCenter,
                                    height: 23,
                                    width: Get.width * .1,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: AppColors.grey),
                                    ),
                                    child: InkButton(
                                        rippleColor: AppColors.grey,
                                        height: 23,
                                        width: Get.width * .1,
                                        borderRadius: 0,
                                        backGroundColor: AppColors.white,
                                        child: "+".text.size(20).make(),
                                        onTap: () {
                                          controller.cart(Types.increment,
                                              model, index, setState);
                                        }),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ).pOnly(top: 5),
                      ),
                      Row(
                        children: [
                          "Sub Total:"
                              .text
                              .size(12)
                              .fontWeight(FontWeight.w700)
                              .make()
                              .pOnly(left: 30, right: 20),
                          "\$${model.subTotalOfItem}"
                              .text
                              .size(12)
                              .color(AppColors.red)
                              .fontWeight(FontWeight.w700)
                              .make(),
                        ],
                      ).pOnly(top: 15),
                    ],
                  )
                ],
              ),
            )),


      ),
    );
  }
}
