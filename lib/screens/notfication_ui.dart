import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';
import '../controllers/notfication_controller.dart';
import '../utils/localisations/app_colors.dart';

class NotificationUi extends StatelessWidget {
  const NotificationUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NotificationController controller = Get.find();

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(
                  Icons.dashboard_rounded,

                  color: AppColors.buttonColorBlue),
              "Notification"
                  .text
                  .size(25)
                  .fontWeight(FontWeight.w700)
                  .color(AppColors.grey)
                  .make(),
              ListView.builder(
                shrinkWrap: true,
                  itemCount: controller.notificationList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return
                      noti(model: controller.notificationList[index]  ).pOnly(top: 20);
                  }),
            ],
          ).pOnly(top: 30, left: 15, right: 15),
        ),
      ),
    );
  }


  Widget noti({
    required MessagesModel model,

  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              height: 40,
              width: Get.width * .2,
              child: CachedNetworkImage(
                imageUrl: model.notificationIcon,
                placeholder: (context, url) => SizedBox(
                  child: LinearProgressIndicator(
                    color: AppColors.accentRipple,
                    minHeight: 10,
                  ),
                ),
                errorWidget: (context, url, error) =>
                const Icon(Icons.error),
              )
            ),
            model.msg.text
                .size(10)
                .fontWeight(FontWeight.w400)
                .make()
                .pOnly(left: 0)
          ],
        ),
      ],
    );
  }
}
