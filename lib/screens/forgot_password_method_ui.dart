import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/forgot_password_method_controller.dart';
import '../controllers/sign_in_controller.dart';
import '../utils/localisations/app_colors.dart';

class ForgotMethodUI extends StatelessWidget {
  const ForgotMethodUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SignInController signInController =Get.find();
    ForgotMethodController controller =Get.find();
    return  SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.white,
        body: signInController.methods==ForgotMethods.viaMail?viaMail(controller):viaPhone(controller),
       
        
      ),
    );
  }
  Widget viaMail(ForgotMethodController controller){
    return
      SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 220,
              width: Get.width,
              decoration: BoxDecoration(
                  color: AppColors.accent,
                  borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  InkButton(
                    backGroundColor: AppColors.accent,
                      height: 30,
                      width: 30,
                      child: Icon(Icons.arrow_back,color: AppColors.white,), onTap: (){
                      Get.offAllNamed(AppRoutes.signIn);
                  }),


                  "Enter your email"
                      .text
                      .size(30)
                      .color(AppColors.white)
                      .make()
                      .pOnly(top: 20),
                  SizedBox(
                    width: Get.width * .8,
                    child:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis."
                        .text
                        .size(12)
                        .color(AppColors.white)
                        .lineHeight(2)
                        .make()
                        .pOnly(right: 0),
                  )
                ],
              ),
            ),
            Form(
                key: controller.emailFormkey,
                child: TextFormField(
                  controller: controller.emailController,
                  validator: (value){
                    return controller.emailValidator();
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.backgroundColor,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: "Your email address",
                      hintStyle: TextStyle(
                          color: AppColors.textGrey)),
                )).pOnly(bottom: 200,top: 74,left: 20,right: 20),

            Container(
              alignment: Alignment.center,
              height: 62,
              width: Get.width * .9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: AppColors.fadedBlue),
              child: "CONTINUE".text.color(AppColors.white).size(14).make(),
            ).pOnly(top: 25).onTap(() {
              controller.onEmailContinueButton(otpMethods: OTPMethods.viaMail);
            }),

          ],
        )
      );
  }
  Widget viaPhone(ForgotMethodController controller){
    return
      SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 220,
              width: Get.width,
              decoration: BoxDecoration(
                  color: AppColors.accent,
                  borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  InkButton(
                      backGroundColor: AppColors.accent,
                      height: 30,
                      width: 30,
                      child: Icon(Icons.arrow_back,color: AppColors.white,), onTap: (){
                    Get.offAllNamed(AppRoutes.signIn);
                  }),
                  "Enter your phone"
                      .text
                      .size(30)
                      .color(AppColors.white)
                      .make()
                      .pOnly(top: 20),
                  SizedBox(
                    width: Get.width * .8,
                    child:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis."
                        .text
                        .size(12)
                        .color(AppColors.white)
                        .lineHeight(2)
                        .make()
                        .pOnly(right: 0),
                  )
                ],
              ),
            ),
            Form(
                key: controller.phoneFormkey,
                child: TextFormField(
                  controller: controller.phoneController,
             validator: (value){
                    return controller.phoneValidator();
             },
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.backgroundColor,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: "Enter phone number",
                      hintStyle: TextStyle(
                          color: AppColors.textGrey)),
                )).pOnly(bottom: 200,top: 74,left: 20,right: 20),

            Container(
              alignment: Alignment.center,
              height: 62,
              width: Get.width * .9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: AppColors.fadedBlue),
              child: "CONTINUE".text.color(AppColors.white).size(14).make(),
            ).pOnly(top: 25).onTap(() {
                 controller.onPhoneContinueButton(otpMethods: OTPMethods.viaPhone);
            }),

          ],
        ),
      );
  }
} 
