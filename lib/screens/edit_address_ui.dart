import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/edit_address_controller.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';


class EditAddressUI extends StatelessWidget {
  const EditAddressUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    EditAddressController controller = Get.find();
    return SafeArea(
      child: Scaffold(
        appBar:
        AppBar(

          elevation: 2,
          leading:  InkButton(

            backGroundColor: AppColors.accent, child: Icon(Icons.arrow_back,color: AppColors.white,),
            onTap: (){
              Get.back();
            },
          ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
          title: "Add delivery address".text.size(15).color(AppColors.grey).make(),
          centerTitle: false,
          backgroundColor: AppColors.white,
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.search,
                color: Colors.grey,
                size: 25,
              ),
              onPressed: () {
              },
            ),

            IconButton(
             icon: Icon(Icons.shopping_cart,size: 20, color: AppColors.grey,),
            onPressed: (){
               Get.toNamed(AppRoutes.cart);
            },
            ).pOnly(right: 8),

          ],
        ),
        backgroundColor: AppColors.white,
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            children: [
              Form(
                key: controller.formKey,
                  child: Column(
                children: [
                  TextFormField(
                    controller: controller.nameController,
                    focusNode: controller.nameFocusNode,
                    onFieldSubmitted: (value) {
                      controller.nameFocusNode.unfocus();
                      controller.phoneFocusNode.requestFocus();
                    },

                    validator: (value){
                      return controller.nameValidator();
                    },
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.grey, width: 1.0),
                      ),
                      labelText: "Full Name(Required)*",
                      labelStyle:
                          TextStyle(fontSize: 15, color: AppColors.fadedBlack),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: AppColors.grey),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: AppColors.grey),
                      ),
                    ),
                  ).pOnly(top: 40, bottom: 30),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    maxLength: 10,
                    validator: (value) {
                      return controller.phoneValidator();
                    },
                    controller: controller.phoneController,
                    focusNode: controller.phoneFocusNode,
                    onFieldSubmitted: (value) {
                      controller.phoneFocusNode.unfocus();
                      controller.pinFocusNode.requestFocus();
                    },


                    decoration: InputDecoration(
                      counterText: "",
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.grey, width: 1.0),
                      ),
                      labelText: "Phone Number(Required)*",
                      labelStyle:
                          TextStyle(fontSize: 15, color: AppColors.fadedBlack),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: AppColors.grey),
                      ),
                      errorBorder: OutlineInputBorder(
                        //<-- SEE HERE
                        borderSide: BorderSide(width: 1, color: AppColors.grey),
                      ),
                    ),
                  ).pOnly(bottom: 10),
                  Align(
                          alignment: Alignment.topLeft,
                          child: "+ Add Alternative Phone Number"
                              .text
                              .color(AppColors.accent)
                              .bold
                              .make()).onTap(() {
                 controller.showAlternativePhoneNumber.value=!controller.showAlternativePhoneNumber.value;
                  }),
                  Obx(
                    ()=> Visibility(
                      visible: controller.showAlternativePhoneNumber.value,
                      child: TextFormField(
                      keyboardType: TextInputType.phone,
                      maxLength: 10,
                      validator: (value) {
                        return controller.alternativePhoneNumberValidator();
                      },
                      controller: controller.alternativePhoneNumberController,
                      decoration: InputDecoration(
                        counterText: "",
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors.grey, width: 1.0),
                        ),
                        labelText: "Add Alternative Phone Number(Required)*",
                        labelStyle:
                        TextStyle(fontSize: 15, color: AppColors.fadedBlack),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: AppColors.grey),
                        ),
                        errorBorder: OutlineInputBorder(
                          //<-- SEE HERE
                          borderSide: BorderSide(width: 1, color: AppColors.grey),
                        ),
                      ),
                    ), )
                        .pOnly(top: 10, bottom: 20),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(

                        width: Get.width * .45,
                        child: TextFormField(
                          keyboardType: TextInputType.number,

                          validator: (value) {
                            return controller.pinCodeValidator();
                          },
                          maxLength: 6,
                          controller: controller.pinCodeController,
                          focusNode: controller.pinFocusNode,
                          onFieldSubmitted: (value) {
                            controller.pinFocusNode.unfocus();
                            controller.stateFocusNode.requestFocus();
                          },
                          decoration: InputDecoration(
                            counterText: "",
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: AppColors.grey, width: 1.0),
                            ),
                            labelText: "PinCode(Required)*",
                            labelStyle: TextStyle(
                                fontSize: 15, color: AppColors.fadedBlack),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 1, color: AppColors.grey),
                            ),
                            errorBorder: OutlineInputBorder(
                              //<-- SEE HERE
                              borderSide:
                                  BorderSide(width: 1, color: AppColors.grey),
                            ),
                          ),
                        ),
                      ),
                      InkButton(
                          height: 50,
                          borderRadius: 5,
                          backGroundColor: AppColors.accent,
                          width: Get.width * .45,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Icon(
                                Icons.my_location_outlined,
                                color: AppColors.white,
                              ),
                              "Use my location"
                                  .text
                                  .color(AppColors.white)
                                  .make(),
                            ],
                          ),
                          onTap: onTap)
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: Get.width * .45,
                        child: TextFormField(
                          validator: (value){
                          return  controller.stateValidator();
                          },
                          controller: controller.stateController,
                          focusNode: controller.stateFocusNode,
                          onFieldSubmitted: (value) {
                            controller.stateFocusNode.unfocus();
                            controller.cityFocusNode.requestFocus();
                          },
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(1),
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: AppColors.grey, width: 1.0),
                            ),
                            labelText: "State(Required)*",
                            labelStyle: TextStyle(
                                fontSize: 15, color: AppColors.fadedBlack),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 1, color: AppColors.grey),
                            ),
                            errorBorder: OutlineInputBorder(
                              //<-- SEE HERE
                              borderSide:
                                  BorderSide(width: 1, color: AppColors.grey),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: Get.width * .45,
                        child: TextFormField(
                          controller: controller.cityController,
                          focusNode: controller.cityFocusNode,
                          onFieldSubmitted: (value) {
                            controller.cityFocusNode.unfocus();
                            controller.houseFousNode.requestFocus();
                          },
                          validator: (v){
                           return controller.cityNameValidator();
                          },
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(1),
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: AppColors.grey, width: 1.0),
                            ),
                            labelText: "City(Required)*",
                            labelStyle: TextStyle(
                                fontSize: 15, color: AppColors.fadedBlack),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 1, color: AppColors.grey),
                            ),
                            errorBorder: OutlineInputBorder(
                              //<-- SEE HERE
                              borderSide:
                                  BorderSide(width: 1, color: AppColors.grey),
                            ),
                          ),
                        ),
                      )
                    ],
                  ).pOnly(top: 20, bottom: 20),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextFormField(
                        keyboardType: TextInputType.streetAddress,

                        validator: (value){
                          return controller.houseNumberValidator();
                        },
                        controller: controller.houseNumberController,
                        focusNode: controller.houseFousNode,
                        onFieldSubmitted: (value) {
                          controller.houseFousNode.unfocus();
                          controller.areaFocusNode.requestFocus();
                        },
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.grey, width: 1.0),
                          ),
                          labelText: "House No.,Building Name(Required)*",
                          labelStyle: TextStyle(
                              fontSize: 15, color: AppColors.fadedBlack),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 1, color: AppColors.grey),
                          ),
                          errorBorder: OutlineInputBorder(
                            //<-- SEE HERE
                            borderSide:
                                BorderSide(width: 1, color: AppColors.grey),
                          ),
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.streetAddress,
                        maxLength: 10,
                        validator: (value) {
                          return controller.roadNameValidator();
                        },
                        controller: controller.roadNameController,
                        focusNode: controller.areaFocusNode,
                        onFieldSubmitted: (value) {
                          controller.areaFocusNode.unfocus();
                          controller.buttonFocusNode.requestFocus();
                        },
                        decoration: InputDecoration(
                          counterText: "",
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.grey, width: 1.0),
                          ),
                          labelText: "Road Name,Area,Colony(Required)*",
                          labelStyle: TextStyle(
                              fontSize: 15, color: AppColors.fadedBlack),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 1, color: AppColors.grey),
                          ),
                          errorBorder: OutlineInputBorder(
                            //<-- SEE HERE
                            borderSide:
                                BorderSide(width: 1, color: AppColors.grey),
                          ),
                        ),
                      ).pOnly(
                        top: 20,
                        bottom: 20,
                      )
                    ],
                  ),
                ],
              )),
              Align(
                  alignment: Alignment.topLeft,
                  child: "Type of address".text.color(AppColors.grey).make()),

              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 40,
                    width: Get.width * .3,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: AppColors.grey,
                      ),
                        borderRadius: BorderRadius.circular(20),
                        color: AppColors.backgroundColor),
                    child: "Home".text.make(),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 40,
                    width: Get.width * .3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: AppColors.backgroundColor),
                    child: "Work".text.make(),
                  ),
                ],
              ).pOnly(bottom: 30,top: 10),
              InkButton(
                  width: Get.width,
                  borderRadius: 15,
                  child: "Save Address"
                      .text
                      .size(15)
                      .color(AppColors.white)
                      .make(),
                  backGroundColor: AppColors.accent,
                  onTap: () {
                    controller.onSaveAddress();
                  }).pOnly(bottom: 20),
            ],
          ).pOnly(left: 15, right: 15,bottom: 20),
        ),
      ),
    );
  }
}
