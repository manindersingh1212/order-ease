// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/payments_controllers.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/custom_widgets.dart';

class PaymentsUi extends StatelessWidget {
  var data = Get.arguments;

  PaymentsUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PaymentsController controller = Get.find();

    return Scaffold(
        backgroundColor: AppColors.backgroundColor,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: AppColors.white,
          title: "Payments"
              .text
              .size(14)
              .fontWeight(FontWeight.w700)
              .color(AppColors.grey)
              .make(),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///first container
              Container(
                height: 196,
                width: Get.width,
                color: AppColors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(
                          "assets/images/ic_location.png",
                          height: 20,
                          width: 20,
                        ),
                        "Shipping Address"
                            .text
                            .color(AppColors.grey)
                            .fontWeight(FontWeight.w400)
                            .size(14)
                            .make(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            "Edit"
                                .text
                                .size(12)
                                .color(AppColors.grey)
                                .fontWeight(FontWeight.w400)
                                .make(),
                          ],
                        ),
                      ],
                    ),
                    Container(
                      height: 2,
                      width: Get.width * .9,
                      color: AppColors.buttonColorGrey,
                    ).pOnly(top: 10, bottom: 14),
                    "Kylie"
                        .text
                        .size(18)
                        .fontWeight(FontWeight.w700)
                        .color(AppColors.textGrey)
                        .make()
                        .pOnly(left: 40),
                    "California Street, Blok 4F No.9\nSan Fransisco\nCalifornia\n0214-0000-0000 "
                        .text
                        .size(
                          12,
                        )
                        .fontWeight(FontWeight.w400)
                        .color(AppColors.textGrey)
                        .make()
                        .pOnly(left: 40),
                  ],
                )
                    .pOnly(left: 25, top: 10, bottom: 17, right: 25)
                    .pOnly(top: 10),
              ).pOnly(top: 10),

              ///second container
              Container(
                height: 100,
                width: Get.width,
                color: AppColors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 57,
                          width: Get.width * .15,
                          decoration: BoxDecoration(
                              color: (AppColors.pink),
                              borderRadius: BorderRadius.circular(5)),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            "Levi’s Jeans"
                                .text
                                .size(14)
                                .fontWeight(FontWeight.w500)
                                .color(AppColors.textGrey)
                                .make(),
                            Row(
                              children: [
                                "Color : Dark Grey"
                                    .text
                                    .size(10)
                                    .fontWeight(FontWeight.w400)
                                    .color(AppColors.grey)
                                    .make(),
                                Container(
                                  height: 10,
                                  width: 2,
                                  color: AppColors.grey,
                                ).pOnly(left: 7, right: 7),
                                "Size:L"
                                    .text
                                    .size(10)
                                    .fontWeight(FontWeight.w400)
                                    .color(AppColors.grey)
                                    .make()
                              ],
                            ),
                            SizedBox(
                              width: Get.width * .6,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  "76"
                                      .text
                                      .size(14)
                                      .fontWeight(FontWeight.w400)
                                      .color(AppColors.buttonColorBlue)
                                      .make(),
                                  "2x"
                                      .text
                                      .size(12)
                                      .fontWeight(FontWeight.w400)
                                      .color(AppColors.grey)
                                      .make()
                                ],
                              ),
                            )
                          ],
                        ).pOnly(left: 19),
                      ],
                    ),
                  ],
                ).pOnly(top: 14, left: 27, right: 27, bottom: 18),
              ).pOnly(top: 10),

              ///third container
              Container(
                height: 90,
                width: Get.width,
                color: AppColors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Delivery Service"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.grey)
                            .make(),
                        "Edit"
                            .text
                            .size(12)
                            .color(AppColors.grey)
                            .fontWeight(FontWeight.w400)
                            .make(),
                      ],
                    ),
                    Container(
                      height: 2,
                      width: Get.width * 9,
                      color: AppColors.buttonColorGrey,
                    ).pOnly(top: 9, bottom: 14),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Express Delivery"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w700)
                            .color(AppColors.textGrey)
                            .make(),
                        "\$12"
                            .text
                            .size(12)
                            .color(AppColors.grey)
                            .fontWeight(FontWeight.w700)
                            .make(),
                      ],
                    ),
                  ],
                ).pOnly(left: 27, right: 27, top: 10, bottom: 10),
              ).pOnly(top: 10),

              ///forth container
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  "Add Description"
                      .text
                      .size(12)
                      .fontWeight(FontWeight.w400)
                      .make(),
                  const TextField(
                    maxLines: 20,
                    minLines: 1,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ],
              ).pOnly(top: 13, bottom: 20, left: 27, right: 27),

              ///fifth container
              Container(
                height: 137,
                width: Get.width,
                color: AppColors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Payment Method"
                            .text
                            .color(AppColors.textGrey)
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .make(),
                        Row(
                          children: [
                            Icon(
                              Icons.payments,
                              color: AppColors.pink,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                "My Pay"
                                    .text
                                    .size(12)
                                    .fontWeight(FontWeight.w400)
                                    .color(AppColors.pink)
                                    .make()
                                    .paddingOnly(left: 5, right: 10),
                                Container(
                                  alignment: Alignment.center,
                                  height: 20,
                                  width: 20,
                                  decoration: BoxDecoration(
                                      color: AppColors.buttonColorBlue,
                                      borderRadius: BorderRadius.circular(15)),
                                  child: const Icon(
                                    Icons.chevron_right,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                )
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                    Container(
                      height: 2,
                      width: Get.width * .9,
                      color: AppColors.buttonColorGrey,
                    ).pOnly(top: 8, bottom: 7),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Subtotals for products"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.grey)
                            .lineHeight(2)
                            .make(),
                        "\$152"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.grey)
                            .make(),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Subtotals for shipping"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.grey)
                            .lineHeight(2)
                            .make(),
                        "\$2"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.grey)
                            .make(),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Total Payment"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.textGrey)
                            .lineHeight(1.5)
                            .make(),
                        "\$232"
                            .text
                            .size(12)
                            .fontWeight(FontWeight.w400)
                            .color(AppColors.textGrey)
                            .make(),
                      ],
                    ),
                  ],
                ).pOnly(left: 27, right: 27, top: 14, bottom: 5),
              ).pOnly(top: 10),

              ///sixth container
              Container(
                height: 60,
                width: Get.width,
                color: AppColors.white,
                child: Obx(
                  () => Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      "Send As a Dropshipper"
                          .text
                          .color(AppColors.textGrey)
                          .fontWeight(FontWeight.w400)
                          .size(12)
                          .make(),
                      Switch(
                          activeColor: AppColors.buttonColorBlue,
                          inactiveThumbColor: AppColors.buttonColorGrey,
                          value: controller.changeButton.value,
                          onChanged: (value) {
                            controller.button();
                          }),
                    ],
                  ).pOnly(top: 15, bottom: 15, left: 27, right: 27),
                ),
              ).pOnly(top: 10),
              Container(
                height: 145,
                width: Get.width,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        "Total"
                            .text
                            .size(14)
                            .fontWeight(FontWeight.w700)
                            .color(AppColors.grey)
                            .make(),
                        "\$154"
                            .text
                            .size(20)
                            .fontWeight(FontWeight.w700)
                            .color(AppColors.pink)
                            .make()
                      ],
                    ),
                  InkButton(
                    backGroundColor: AppColors.buttonColorBlue,
                    height: 62,
                    child: "BUY NOW"
                        .text
                        .size(14)
                        .color(AppColors.white)
                        .fontWeight(FontWeight.w700)
                        .make(),
                    onTap: () {
                      showModalBottomSheet<void>(
                        backgroundColor: AppColors.white,
                        context: context,
                        builder: (BuildContext context) {
                          return SizedBox(
                            height: 385,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 5,
                                    width: Get.width * .2,
                                    decoration: BoxDecoration(
                                        color: AppColors.grey,
                                        borderRadius:
                                        BorderRadius.circular(5)),
                                  ).pOnly(top: 12),
                                  "Enter The My Pay Pin"
                                      .text
                                      .size(14)
                                      .fontWeight(FontWeight.w400)
                                      .color(AppColors.grey)
                                      .make()
                                      .pOnly(top: 30, bottom: 35),
                                  Form(
                                      key: controller.formkey,
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Container(
                                            alignment: Alignment.center,
                                            height: 63,
                                            width: 63,
                                            decoration: BoxDecoration(
                                                color: AppColors.grey,
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextFormField(
                                                cursorHeight: 60,
                                                onChanged: (value) {
                                                  controller.deleteOTP(
                                                      fieldType: FieldType
                                                          .first);
                                                },
                                                obscureText: true,
                                                maxLength: 1,
                                                keyboardType:
                                                TextInputType.number,
                                                controller: controller
                                                    .firstFieldController,
                                                focusNode: controller
                                                    .firstFocusNode,
                                                // cursorHeight: 40,
                                                decoration:
                                                const InputDecoration(
                                                    counterText: "",
                                                    border:
                                                    InputBorder
                                                        .none),
                                                style: const TextStyle(
                                                    fontSize: 36))
                                                .pOnly(left: 25, bottom: 2),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            height: 63,
                                            width: 63,
                                            decoration: BoxDecoration(
                                                color: AppColors.grey,
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextFormField(
                                                onChanged: (value) {
                                                  controller.deleteOTP(
                                                      fieldType: FieldType
                                                          .second);
                                                },
                                                obscureText: true,
                                                maxLength: 1,
                                                keyboardType: TextInputType
                                                    .number,
                                                controller: controller
                                                    .secondFieldController,
                                                focusNode: controller
                                                    .secondFocusNode,

                                                // cursorHeight: 40,
                                                decoration:
                                                const InputDecoration(
                                                    counterText: "",
                                                    hintStyle:
                                                    TextStyle(
                                                        fontSize:
                                                        14),
                                                    border:
                                                    InputBorder
                                                        .none),
                                                style: const TextStyle(
                                                    fontSize: 36))
                                                .pOnly(left: 25, bottom: 2),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            height: 63,
                                            width: 63,
                                            decoration: BoxDecoration(
                                                color: AppColors.grey,
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextFormField(
                                                onChanged: (value) {
                                                  controller.deleteOTP(
                                                      fieldType: FieldType
                                                          .third);
                                                },
                                                obscureText: true,
                                                maxLength: 1,
                                                keyboardType:
                                                TextInputType.number,
                                                controller: controller
                                                    .thirdFieldController,
                                                focusNode: controller
                                                    .thirdFocusNode,
                                                // cursorHeight: 40,
                                                decoration:
                                                const InputDecoration(
                                                    counterText: "",
                                                    border:
                                                    InputBorder
                                                        .none),
                                                style: const TextStyle(
                                                    fontSize: 36))
                                                .pOnly(left: 25, bottom: 2),
                                          ),
                                          Container(
                                              alignment: Alignment.center,
                                              height: 63,
                                              width: 63,
                                              decoration: BoxDecoration(
                                                  color: AppColors.grey,
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextFormField(
                                                  onChanged: (value) {
                                                    controller.deleteOTP(
                                                        fieldType:
                                                        FieldType
                                                            .fourth);
                                                  },
                                                  obscureText: true,
                                                  maxLength: 1,
                                                  keyboardType:
                                                  TextInputType
                                                      .number,
                                                  controller: controller
                                                      .fourthFieldController,
                                                  focusNode: controller
                                                      .fourthFocusNode,
                                                  // cursorHeight: 40,
                                                  decoration:
                                                  const InputDecoration(
                                                      counterText: "",
                                                      border:
                                                      InputBorder
                                                          .none),
                                                  style: const TextStyle(
                                                      fontSize: 36))
                                                  .pOnly(
                                                  left: 25, bottom: 2)),
                                        ],
                                      )).pOnly(bottom: 0),
                                  Container(
                                    alignment: Alignment.centerRight,
                                    child: "Forgot pin?"
                                        .text
                                        .size(12)
                                        .fontWeight(FontWeight.w400)
                                        .color(AppColors.red)
                                        .make()
                                        .pOnly(top: 15, bottom: 40),
                                  ),
                                InkButton(child: "BUY".text.size(14).color(AppColors.white).fontWeight(FontWeight.w700).make(),backGroundColor: AppColors.buttonColorBlue, onTap: (){
                                  controller
                                      .onConfirmButtonSubmitted(context);

                                })
                                  


                                ]).pOnly(bottom: 20, left: 30, right: 30),
                          );
                        },
                      );

                    },
                  ).pOnly(top: 15),


                  ],
                ).pOnly(left: 27, right: 27, top: 20, bottom: 0),
              ).pOnly(top: 10)
            ],
          ),
        ));
  }
}
