
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/image_paths.dart';

class SplashUI extends StatelessWidget {
  const SplashUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
    body: Center(
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
       Lottie.asset(ImagesPaths.splash,fit: BoxFit.cover),
        ],
      )
    ),
    );
  }
}
