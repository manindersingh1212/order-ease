import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/choose_language_controller.dart';
import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/localisations/image_paths.dart';

class ChooseLanguageUi extends StatelessWidget {
  const ChooseLanguageUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ChooseLanguageController controller = Get.find();
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            elevation: 2,
            leading: InkButton(
              backGroundColor: AppColors.accent,
              child: Icon(
                Icons.arrow_back,
                color: AppColors.white,
              ),
              onTap: () {
                Get.back();
              },
            ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
            title: "Choose Language".text.size(17).color(AppColors.grey).make(),
            centerTitle: true,
            backgroundColor: AppColors.white,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  // height: Get.width,
                  height: 590,
                  width: Get.width * .99,
                  //List of languages
                  child: ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: controller.languageList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return items(
                                context: context,
                                list: controller.languageList[index],
                                controller: controller,
                                index: index)
                            .py(10);
                      }),
                ),
                const Divider(
                  color: Colors.grey,
                ),
                Obx(() =>
                    //Continue button
                    Container(
                      alignment: Alignment.center,
                      height: 46,
                      width: Get.width * .99,
                      decoration: BoxDecoration(
                        color: controller.selectedColor().value,
                      ),
                      child: "CONTINUE".text.white.make(),
                    ).pOnly(left: 8, right: 8, bottom: 7).onTap(() {
                      controller.onContinueButton();
                    }))
              ],
            ),
          )),
    );
  }

//Widget of language  list
  Widget items(
      {required BuildContext context,
      required list,
      required ChooseLanguageController controller,
      required int index}) {
    return Obx(() => Column(
          children: [
            Container(
                alignment: Alignment.centerLeft,
                height: 70,
                width: Get.width * .9,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: controller.selectedItem.value == index
                        ? Border.all(color: Colors.blue.shade800)
                        : Border.all(color: Colors.grey.shade400, width: 2)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Transform.scale(
                          scale: 1.2,
                          child: Checkbox(
                            checkColor: Colors.white,
                            value: controller.selectedItem.value == index
                                ? true
                                : false,
                            shape: const CircleBorder(),
                            onChanged: (value) {
                              controller.checkBox(index);
                            },
                          ),
                        ),
                        Column(
                          children: [
                            "$list"
                                .text
                                .size(15)
                                .fontWeight(FontWeight.w500)
                                .make()
                                .pOnly(top: 17),
                            "$list"
                                .text
                                .size(10)
                                .color(Colors.grey)
                                .make()
                                .pOnly(right: 10),
                          ],
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Image.asset(
                          ImagesPaths.ic_home,
                          height: 40,
                          color: AppColors.grey,
                        ).pOnly(top: 5, right: 30),
                      ],
                    )
                  ],
                )).pOnly(left: 10, right: 10).onTap(() {
              controller.checkBox(index);
            }),
            Visibility(
                visible: controller.selectedItem.value == index ? true : false,
                child: SizedBox(
                  width: Get.width * .9,
                  child:
                      "A random paragraph can also be an excellent project that the writer is trying to complete. first place."
                          .text
                          .color(Colors.grey)
                          .make(),
                ))
          ],
        ));
  }
}
