import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/localisations/image_paths.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/main_home_controller.dart';
import '../utils/localisations/app_colors.dart';

class MainHomeUI extends StatelessWidget {
  const MainHomeUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainHomeController controller = Get.find();
    return
      Scaffold(
        backgroundColor: AppColors.white,
        body: Obx(() => Center(
              child: MainHomeController.widgetOptions
                  .elementAt(controller.selectedIndex.value),
            )),
        bottomNavigationBar: Container(
          alignment: Alignment.center,
          height: 60,
          width: 313,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: AppColors.grey,
                blurRadius: 4.0,
              ),
            ],
            color: AppColors.accent,
            borderRadius: BorderRadius.circular(50),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkButton(
                  height: 40,
                  width: 60,
                  backGroundColor: AppColors.accent,
                  child: bottomNavigationItems(
                          showBadge: false,
                          controller: controller,
                          navigationIcon: ImagesPaths.ic_home,
                          currentIndex: 0)
                      .pOnly(top: 5),
                  onTap: () {
                    controller.selectedIndex.value = 0;

                  }),
              InkButton(
                  height: 40,
                  width: 60,
                  backGroundColor: AppColors.accent,
                  child: bottomNavigationItems(
                          showBadge: true,
                          controller: controller,
                          navigationIcon: ImagesPaths.ic_notification,
                          currentIndex: 1)
                      .pOnly(top: 5),
                  onTap: () {
                    controller.selectedIndex.value = 1;

                  }),
              InkButton(
                  backGroundColor: AppColors.accent,
                  height: 40,
                  width: 60,
                  child: bottomNavigationItems(
                    showBadge: false,
                    controller: controller,
                    navigationIcon: ImagesPaths.ic_profile1,
                    currentIndex: 2,
                  ).pOnly(top: 5),
                  onTap: () {
                    controller.selectedIndex.value = 2;

                  }),
            ],
          ).pOnly(top: 1),
        ).pOnly(bottom: 28, left: 31, right: 31));
  }

  Widget bottomNavigationItems(
      {required MainHomeController controller,
      required String navigationIcon,
      required bool showBadge,
      required int currentIndex}) {
    return Obx(() => badges.Badge(
          position: badges.BadgePosition.custom(top: -10, start: 11),
          badgeAnimation: const badges.BadgeAnimation.slide(
              ),
          showBadge: showBadge,
          badgeStyle: badges.BadgeStyle(
            badgeColor: AppColors.red,
          ),
          badgeContent: const Text(
            "8",
            style: TextStyle(color: Colors.white, fontSize: 12),
          ),
          child: Column(
            children: [
              Image.asset(
                navigationIcon,
                height: 25,
                color: controller.selectedIndex.value == currentIndex
                    ? AppColors.buttonColorBlue
                    : AppColors.grey,
              ),
            ],
          ),
        )).onTap(() {
      controller.selectedIndex.value = currentIndex;
    });
  }
}
