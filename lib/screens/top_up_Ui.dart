// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/top_up_controller.dart';
import 'package:order_ease_e_com/utils/localisations/image_paths.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class TopUpUi extends StatelessWidget {
  TopUpController controller = Get.find();

   TopUpUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return  Scaffold(

      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColors.white,
        leading: InkButton(
            height: 5,
            width: 5,
            borderRadius: 30,
            backGroundColor: AppColors.accent,
            child: Icon(
              Icons.arrow_back,
              color: AppColors.white,
            ),
            onTap: () {
              Get.offAllNamed(AppRoutes.mainHome);
            }).pOnly(left: 10, top: 10, bottom: 10, right: 10),

        title: "Top Up"
            .text
            .size(14)
            .fontWeight(FontWeight.w700)
            .color(AppColors.grey)
            .make(),
      ),
      body: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(height: 230,width: Get.width*.99,
              decoration: BoxDecoration(

                color: AppColors.backgroundColor,
                borderRadius: BorderRadius.circular(20)

              ),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  "Nominal input".text.size(14).fontWeight(FontWeight.w400).color(AppColors.grey).make(),
              TextField(
                controller: controller.textController,
                      autofocus: false,
                      style: const TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        prefixText: "\$",
                        prefixStyle: TextStyle(
                          fontSize: 20,
                          color: AppColors.grey
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        hintText: '\$0',
                        hintStyle: TextStyle(fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: AppColors.grey),
                        contentPadding:
                        const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.white),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ).pOnly(top: 12,bottom: 25),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(alignment: Alignment.center,
                        height: 55,width: Get.width*0.16,
                        decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child:  Image.asset(
                          "assets/images/ic_coin.png",
                          height: 20,width: 20,
                        ),

                      ).onTap(() { controller.onSelectDollar(value: SelectedDollarValue.ten);}),
                      Container(alignment: Alignment.center,
                        height: 55,width: Get.width*0.16,
                        decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8)
                        ),
                        child:  Image.asset(
                          ImagesPaths.ic_coin,
                          height: 20,width: 20,
                        ),

                      ).onTap(() { controller.onSelectDollar(value: SelectedDollarValue.fifty);}),
                      Container(alignment: Alignment.center,
                        height: 55,width: Get.width*0.16,
                        decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8)
                        ),
                        child:  Image.asset(
                          "assets/images/ic_coin.png",
                          height: 20,width: 20,
                        ),

                      ).onTap(() { controller.onSelectDollar(value: SelectedDollarValue.hundred);}),
                      Container(alignment: Alignment.center,
                        height: 55,width: Get.width*0.16,
                        decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8)
                        ),
                        child:  Image.asset(
                          "assets/images/ic_coin.png",
                          height: 20,width: 20,
                        ),

                      ).onTap(() { controller.onSelectDollar(value:SelectedDollarValue.twoHundred);}),




                  ],),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    "\$${controller.tenRupees.value}".text.size(12).fontWeight(FontWeight.w700).color(AppColors.buttonColorBlue).make(),
                      "\$50".text.size(12).fontWeight(FontWeight.w700).color(AppColors.buttonColorBlue).make(),
                      "\$100".text.size(12).fontWeight(FontWeight.w700).color(AppColors.buttonColorBlue).make(),
                      "\$200".text.size(12).fontWeight(FontWeight.w700).color(AppColors.buttonColorBlue).make(),




                    ],).pOnly(left: 15,right: 15,top: 10),




              ],).pOnly(left: 25,top: 15,right: 25,bottom: 20),
            ).pOnly(left: 10,right: 10),
            Container(
              alignment: Alignment.center,
              height: 90,width: Get.width,
              decoration: BoxDecoration(

                color: AppColors.backgroundColor,

                borderRadius: BorderRadius.circular(10),
             ),
              child: Column(mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: [
                      Image.asset(ImagesPaths.ic_card,height: 30,width: 30,),
                      Column(crossAxisAlignment: CrossAxisAlignment.start,

                        children: [

                          "Add a debit card".text.size(14).fontWeight(FontWeight.w700).lineHeight(2).color(AppColors.textGrey).make(),
                          "Can this balance directly from here".text.fontWeight(FontWeight.w400).size(10).color(AppColors.grey).make(),
                        ],),
                      Container(
                        alignment: Alignment.center,
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                            color: AppColors.buttonColorBlue,
                            borderRadius:
                            BorderRadius.circular(15)),
                        child: const Icon(
                          Icons.chevron_right,
                          color: Colors.white,
                          size: 20,
                        ),
                      )


                    ],),
                ],
              ).pOnly(top: 20,bottom: 18,left: 15,right: 20),


            ).onTap(() {
              Get.toNamed(AppRoutes.debitCard) ;
            }).pOnly(top: 20,left: 10,right: 10),
            InkButton(
                backGroundColor: AppColors.grey,
                child: "CONTINUE".text.size(14).color(AppColors.white).fontWeight(FontWeight.w700).make(), onTap: (){

            }).pOnly(top: 250,left: 20,right: 20)


          ],
        ),
      ),
    );
  }
}
