import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

class SettingsUI extends StatelessWidget {
  const SettingsUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar:
        AppBar
          (

          elevation: 2,
          leading:  InkButton(

            backGroundColor: AppColors.accent, child: Icon(Icons.arrow_back,color: AppColors.white,),
            onTap: (){
              Get.back();
            },
          ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
          title: "Settings".text.size(17).color(AppColors.grey).make(),
          centerTitle: true,
          backgroundColor: AppColors.white,

        ),
        backgroundColor: AppColors.white,
        body: Column(
          children: [
            settingsItems(titles:"Choose language",leadingIconWidget: Icon(Icons.language,color: AppColors.fadedBlack),onClick: (){
              Get.toNamed(AppRoutes.chooseLanguage);
            }),
            settingsItems(titles: "Notification settings",leadingIconWidget: Icon(Icons.notification_important_outlined,color: AppColors.fadedBlack),onClick: (){
            Get.toNamed(AppRoutes.notificationsSettings);

            }),
            settingsItems(titles: "Logout",leadingIconWidget: Icon(Icons.follow_the_signs_outlined,color: AppColors.fadedBlack,),onClick: (){


                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      contentPadding: const EdgeInsets.all(16.0),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          'Forgot Password'.text.size(14).bold.make(),
                          InkButton(
                              rippleColor: AppColors.accentRipple,
                              backGroundColor: AppColors.backgroundColor,
                              height: 23,
                              width: 20,
                              child: const Icon(CupertinoIcons.multiply_circle), onTap: (){
                            Navigator.of(context).pop();
                          }),
                        ],
                      ),
                      content: SizedBox(
                        height: 80,
                        width: Get.width,
                        child:
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkButton(
                              rippleColor: AppColors.accentRipple,

                              backGroundColor: AppColors.accent,
                              height: 40,
                              width: Get.width*.3,


                              child:"Cancel".text.fontWeight(FontWeight.w400).color(AppColors.white).make(),
                              onTap: (){
                                Navigator.of(context).pop();
                              },

                            ),
                            InkButton(
                              rippleColor: AppColors.accentRipple,
                              backGroundColor: AppColors.accent,

                              height: 40,
                              width: Get.width*.3,


                              child:"Logout".text.fontWeight(FontWeight.w400).color(AppColors.white).make(),
                              onTap: (){
                                Navigator.of(context).pop();
                                final token=GetStorage();
                                token.write("accessToken", "");
                                printData("Deleting Token");
                                Get.offAllNamed(AppRoutes.signIn);
                              },

                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );

            }),
          ],
        ),
      ),
    );
  }
  Widget settingsItems({required String titles,required Widget leadingIconWidget,required Function onClick }){
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          height: 50,
          width: Get.width,
          decoration: const BoxDecoration(

          ),
          child:ListTile(
            leading: leadingIconWidget,
            title: titles.text.make(),
            trailing: Icon(Icons.navigate_next,color: AppColors.fadedBlack)),
        ).onTap(() {
          onClick();
        }),
        const Divider()
      ],
    );
  }
}
