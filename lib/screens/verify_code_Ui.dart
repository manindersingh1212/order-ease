import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/verfiy_code_Controller.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/forgot_password_controller.dart';
import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class VerifyCodeUi extends StatelessWidget {
  const VerifyCodeUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    VerifyController controller = Get.find();

    return  Scaffold(


      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColors.white,
        leading: InkButton(
            height: 5,
            width: 5,
            borderRadius: 30,
            backGroundColor: AppColors.accent,
            child: Icon(
              Icons.arrow_back,
              color: AppColors.white,
            ),
            onTap: () {
              Get.toNamed(AppRoutes.cart);
            }).pOnly(left: 10, top: 10, bottom: 10, right: 10),

        title: "Top Up"
            .text
            .size(14)
            .fontWeight(FontWeight.w700)
            .color(AppColors.grey)
            .make(),
      ),
      body: SingleChildScrollView(
        child: Column(mainAxisAlignment: MainAxisAlignment.start,
          children: [
            "Confirm code via phone number".text.size(20).fontWeight(FontWeight.w500).color(AppColors.grey).make(),
            "My Pay sends a code via sms to your phone\nnumber + 1424-XXXX-XX67, enter the code !".text.size(14).fontWeight(FontWeight.w400).color(AppColors.grey).lineHeight(1.5).make(),



                  Form(
                      key: controller.formkey,
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            height: 63,
                            width: 63,
                            decoration: BoxDecoration(
                                color: AppColors.grey,
                                borderRadius:
                                BorderRadius.circular(
                                    10)),
                            child: TextFormField(
                                onChanged: (value) {
                                  controller.deleteOTP(
                                      fieldType: FieldType
                                          .first);
                                },
                                obscureText: true,
                                maxLength: 1,
                                keyboardType:
                                TextInputType.number,
                                controller: controller
                                    .firstFieldController,
                                focusNode: controller
                                    .firstFocusNode,
                                // cursorHeight: 40,
                                decoration:
                                const InputDecoration(
                                    counterText: "",
                                    border:
                                    InputBorder
                                        .none),
                                style: const TextStyle(
                                    fontSize: 40))
                                .pOnly(left: 25, bottom: 5),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: 63,
                            width: 63,
                            decoration: BoxDecoration(
                                color: AppColors.grey,
                                borderRadius:
                                BorderRadius.circular(
                                    10)),
                            child: TextFormField(
                                onChanged: (value) {
                                  controller.deleteOTP(
                                      fieldType: FieldType
                                          .second);
                                },
                                obscureText: true,
                                maxLength: 1,
                                keyboardType: TextInputType
                                    .number,
                                controller: controller
                                    .secondFieldController,
                                focusNode: controller
                                    .secondFocusNode,

                                // cursorHeight: 40,
                                decoration:
                                const InputDecoration(
                                    counterText: "",
                                    hintStyle:
                                    TextStyle(
                                        fontSize:
                                        14),
                                    border:
                                    InputBorder
                                        .none),
                                style: const TextStyle(
                                    fontSize: 40))
                                .pOnly(left: 25, bottom: 5),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: 63,
                            width: 63,
                            decoration: BoxDecoration(
                                color: AppColors.grey,
                                borderRadius:
                                BorderRadius.circular(
                                    10)),
                            child: TextFormField(
                                onChanged: (value) {
                                  controller.deleteOTP(
                                      fieldType: FieldType
                                          .third);
                                },
                                obscureText: true,
                                maxLength: 1,
                                keyboardType:
                                TextInputType.number,
                                controller: controller
                                    .thirdFieldController,
                                focusNode: controller
                                    .thirdFocusNode,
                                // cursorHeight: 40,
                                decoration:
                                const InputDecoration(
                                    counterText: "",
                                    border:
                                    InputBorder
                                        .none),
                                style: const TextStyle(
                                    fontSize: 40))
                                .pOnly(left: 25, bottom: 5),
                          ),
                          Container(
                              alignment: Alignment.center,
                              height: 63,
                              width: 63,
                              decoration: BoxDecoration(
                                  color: AppColors.grey,
                                  borderRadius:
                                  BorderRadius.circular(
                                      10)),
                              child: TextFormField(
                                  onChanged: (value) {
                                    controller.deleteOTP(
                                        fieldType:
                                        FieldType
                                            .fourth);
                                  },
                                  obscureText: true,
                                  maxLength: 1,
                                  keyboardType:
                                  TextInputType
                                      .number,
                                  controller: controller
                                      .fourthFieldController,
                                  focusNode: controller
                                      .fourthFocusNode,
                                  // cursorHeight: 40,
                                  decoration:
                                  const InputDecoration(
                                      counterText: "",
                                      border:
                                      InputBorder
                                          .none),
                                  style: const TextStyle(
                                      fontSize: 40))
                                  .pOnly(
                                  left: 25, bottom: 5)),
                        ],
                      )).pOnly(top: 100),
            "This code will expire on 5 minutes".text.fontWeight(FontWeight.w400).size(12).color(AppColors.buttonColorBlue).make().pOnly(top: 100,bottom: 100),
            InkButton(child: "VERIFY CODE".text.fontWeight(FontWeight.w700).size(14).color(AppColors.white).make(),backGroundColor: AppColors.buttonColorBlue, onTap: (){                                      controller.onConfirmButtonSubmitted(context);
            }),
            InkButton(child: "RESEND CODE".text.fontWeight(FontWeight.w700).size(14).color(AppColors.white).make(),backGroundColor: AppColors.buttonColorGrey, onTap: onTap).pOnly(bottom: 20,top: 15)



          ]).
        pOnly(top: 40, left: 10, right: 10),
      ),




    );

  }
}
