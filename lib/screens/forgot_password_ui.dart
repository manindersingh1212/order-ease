import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/sign_up_controller.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/forgot_password_controller.dart';
import '../controllers/forgot_password_method_controller.dart';
import '../utils/localisations/app_colors.dart';

class ForgotPasswordUI extends StatelessWidget {
  const ForgotPasswordUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ForgotPasswordController controller = Get.find();
    ForgotMethodController forgotMethodController = Get.find();
    SignUpController signUpController = Get.find();
    return Scaffold(
      backgroundColor: AppColors.white,
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 256,
              width: Get.width,
              decoration: BoxDecoration(
                  color: AppColors.accent,
                  borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 50,
                  ),
                  InkButton(
                    height: 30,
                    width: 30,
                    backGroundColor: AppColors.accent,
                    rippleColor: AppColors.accentRipple,
                    child:  Icon(Icons.keyboard_backspace,color: AppColors.white,),
                    onTap: (){
                      Get.offAllNamed(AppRoutes.signIn);
                    }
                  ),
                  "Forgot password"
                      .text
                      .size(30)
                      .color(AppColors.white)
                      .make()
                      .pOnly(top: 20),
                  SizedBox(
                    width: Get.width * .8,
                    child:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis."
                            .text
                            .size(12)
                            .color(AppColors.white)
                            .lineHeight(2)
                            .make()
                            .pOnly(right: 0),
                  )
                ],
              ),
            ),
            Form(
                key: controller.formkey,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      height: 63,
                      width: 63,
                      decoration: BoxDecoration(
                          color: AppColors.backgroundColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextFormField(
                        onChanged: (value) {
                          controller.deleteOTP(fieldType: FieldType.first);
                        },

                        obscureText: true,
                        maxLength: 1,
                        keyboardType: TextInputType.number,
                        controller: controller.firstFieldController,
                        focusNode: controller.firstFocusNode,
                        // cursorHeight: 40,
                        decoration: const InputDecoration(
                            counterText: "", border: InputBorder.none),
                      ).pOnly(left: 25),
                    ),
                    Container(
                      alignment: Alignment.center,
                      height: 63,
                      width: 63,
                      decoration: BoxDecoration(
                          color: AppColors.backgroundColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextFormField(
                        onChanged: (value) {
                          controller.deleteOTP(fieldType: FieldType.second);
                        },

                        obscureText: true,
                        maxLength: 1,
                        keyboardType: TextInputType.number,
                        controller: controller.secondFieldController,
                        focusNode: controller.secondFocusNode,

                        // cursorHeight: 40,
                        decoration: const InputDecoration(
                            counterText: "",
                            hintStyle: TextStyle(fontSize: 14),
                            border: InputBorder.none),
                      ).pOnly(left: 25),
                    ),
                    Container(
                      alignment: Alignment.center,
                      height: 63,
                      width: 63,
                      decoration: BoxDecoration(
                          color: AppColors.backgroundColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextFormField(
                        onChanged: (value) {
                          controller.deleteOTP(fieldType: FieldType.third);
                        },

                        obscureText: true,
                        maxLength: 1,
                        keyboardType: TextInputType.number,
                        controller: controller.thirdFieldController,
                        focusNode: controller.thirdFocusNode,
                        // cursorHeight: 40,
                        decoration: const InputDecoration(
                            counterText: "", border: InputBorder.none),
                      ).pOnly(left: 25),
                    ),
                    Container(
                        alignment: Alignment.center,
                        height: 63,
                        width: 63,
                        decoration: BoxDecoration(
                            color: AppColors.backgroundColor,
                            borderRadius: BorderRadius.circular(10)),
                        child: TextFormField(
                          onChanged: (value) {
                            controller.deleteOTP(fieldType: FieldType.fourth);
                          },

                          obscureText: true,
                          maxLength: 1,
                          keyboardType: TextInputType.number,
                          controller: controller.fourthFieldController,
                          focusNode: controller.fourthFocusNode,
                          // cursorHeight: 40,
                          decoration: const InputDecoration(
                              counterText: "", border: InputBorder.none),
                        ).pOnly(left: 25)),
                  ],
                )).pOnly(top: 80, bottom: 74),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                forgotMethodController.optType == OTPMethods.viaMail
                    ? "Code was send your email"
                        .text
                        .size(12)
                        .fontWeight(FontWeight.w400)
                        .make()
                    : "Code was send your phone"
                        .text
                        .size(12)
                        .fontWeight(FontWeight.w400)
                        .make()
              ],
            ),
            forgotMethodController.optType == OTPMethods.viaPhone
                ? forgotMethodController.phoneController.text.text
                    .size(12)
                    .fontWeight(FontWeight.w400)
                    .color(AppColors.blue)
                    .make()
                    .centered()
                    .pOnly(bottom: 77, top: 5):forgotMethodController.optType == OTPMethods.viaMail?
            forgotMethodController.emailController.text.text
                    .size(12)
                    .fontWeight(FontWeight.w400)
                    .color(AppColors.blue)
                    .make()
                    .centered()
                    .pOnly(bottom: 77, top: 5):
            signUpController.emailController.text.text
                .size(12)
                .fontWeight(FontWeight.w400)
                .color(AppColors.blue)
                .make()
                .centered()
                .pOnly(bottom: 77, top: 5),
                            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(text: 'This code will expire on'),
                  TextSpan(
                    text: ' 5 minutes',
                    style: TextStyle(
                        color: AppColors.blue,
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 62,
              width: Get.width * .9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: AppColors.fadedBlue),
              child: "VERIFY CODE".text.color(AppColors.white).size(14).make(),
            ).pOnly(top: 25).onTap(() {
              controller.onConfirmButtonSubmitted(forgotMethodController);
            }),
            Container(
              alignment: Alignment.center,
              height: 62,
              width: Get.width * .9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: AppColors.buttonColorGrey),
              child: "RESEND CODE".text.color(AppColors.white).size(14).make(),
            ).pOnly(top: 16, bottom: 40)
          ],
        ),
      ),
    );
  }
}
