import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/introduction_controller.dart';
import '../utils/localisations/app_colors.dart';

class IntroductionUi extends StatelessWidget {
  const IntroductionUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IntroductionController controller = Get.find();

    return IntroductionScreen(
      globalBackgroundColor: Colors.white,
      pages: [
        for (int i = 0; i <= controller.introData.length - 1; i++)
          _pageViewModel(
              title: controller.introData[i].title,
              imagePath: controller.introData[i].imagePath,
              body: controller.introData[i].body)
      ],
      showSkipButton: true,
      showBackButton: false,
      back: "".text.color(Colors.grey).make(),
      skip: 'Skip'.text.color(AppColors.grey)
          .make().pOnly(top: 148),
      next: Container(
          alignment: Alignment.center,
          height: 40,
          width: 150,
          decoration: BoxDecoration(
              color: AppColors.buttonColorBlue,
              borderRadius: BorderRadius.circular(55)),
          child: "Next".text.color(Colors.white).make())
          .pOnly(top: 148),
      onDone: () {
        Get.offAllNamed(AppRoutes.signIn);
      },
      done: Container(
          alignment: Alignment.center,
          height: 40,
          width: 150,
          decoration: BoxDecoration(
              color: AppColors.buttonColorBlue,
              borderRadius: BorderRadius.circular(55)),
          child: "Next".text.color(Colors.white).make())
          .pOnly(top: 148),
      dotsDecorator:  DotsDecorator(
        size: const Size(8.0, 8.0),
        color: const Color(0xFFBDBDBD),
        activeColor: AppColors.buttonColorBlue,
        activeSize: const Size(30.0, 8.0),
        activeShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }

  PageViewModel _pageViewModel({
    required String imagePath,
    required String title,
    required String body,
  }) {
    return PageViewModel(
      // title: title,
      titleWidget: ""
          .text
          .fontWeight(FontWeight.w500)
          .size(18)
          .color(Colors.black)
          .make(),
      bodyWidget: Container(
        alignment: Alignment.center,
        width: Get.width,
        height: 540,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                imagePath,
                height: 200,
                width: Get.width*9,
                // height: Get.height * .230,
                // width: Get.width*.256,
              ),
              title.text
                  .fontWeight(FontWeight.w500)
                  .size(18)
                  .color(AppColors.textGrey)
                  .make()
                  .pOnly(top: 79),
              body.text
                  .size(14)
                  .fontWeight(FontWeight.w400)
                  .align(TextAlign.center)
                  .color(AppColors.grey)
                  .lineHeight(1.5)
                  .make()
                  .pOnly(top: 39),
            ],
          ),
        ),
      ),
    );
  }
}
