import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/search_page_controller.dart';
import 'package:order_ease_e_com/utils/localisations/app_strings.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class SearchPageUi extends StatelessWidget {
  const SearchPageUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SearchPageController controller =Get.find();
    return SafeArea(
      child: Scaffold(
          body: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 45,
                    width: Get.width * .7,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: 45,
                          width: Get.width * .4,
                          child: TextFormField(
                            autofocus: true,
                            textInputAction: TextInputAction.search,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Search",
                                hintStyle: TextStyle(color: AppColors.grey)),
                          ),
                        ).pOnly(left: 20),
                        InkButton(
                            height: 45,
                            width: 60,
                            backGroundColor: AppColors.accent,
                            child: Icon(
                              Icons.search,
                              color: AppColors.white,
                            ),
                            onTap: (){

                            }
                        ),
                      ],
                    ),
                  ),
                  InkButton(
                      height: 45,
                      width: Get.width*.12,
                      backGroundColor: AppColors.accent,
                      child: Icon(
                        Icons.shopping_cart_outlined,
                        color: AppColors.white,
                      ),
                      onTap: (){
                        Get.toNamed(AppRoutes.cart);
                      }
                  )
                ],
              ).pOnly(left: 20, right: 25, bottom: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  "${controller.numberOfItemsFound} Items Found"
                      .text
                      .size(12)
                      .fontWeight(FontWeight.w400)
                      .color(AppColors.buttonColorGrey)
                      .make(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(Icons.menu, color: (AppColors.buttonColorBlue)),
                      "Filter"
                          .text
                          .size(12)
                          .fontWeight(FontWeight.w400)
                          .color(AppColors.buttonColorGrey)
                          .make()
                          .pOnly(left: 10),
                    ],
                  )
                ],
              ).pOnly(left: 30, right: 30,bottom: 20),
              GridView.count(
                physics: const NeverScrollableScrollPhysics(),
                  crossAxisCount: 2,
                  crossAxisSpacing: 2,
                  mainAxisSpacing: 0,
                  shrinkWrap: true,
                  children: List.generate(20, (index) {
                    return searchPage(text: "Orange Summer", text2: '78');
                  })),
            ]),
          )),
    );
  }

  Widget searchPage({required String text, required String text2}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.bottomRight,
          height: Get.height*.15,
          width: Get.width*.5,
          decoration: BoxDecoration(
              color: AppColors.buttonColorGrey,
              borderRadius: BorderRadius.circular(15)),
          child: Icon(
            Icons.favorite_border,
            color: AppColors.white,
          ).pOnly(right: 5,bottom: 5),
        ),
        text.text.size(12).fontWeight(FontWeight.w500).make().pOnly(left: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: Get.width * 0.25,
              child: RatingBarIndicator(
                rating: 3.5,
                itemBuilder: (context, index) => Icon(
                  Icons.star,
                  color: AppColors.yellow,
                ),
                itemCount: 5,
                itemSize: 15.0,
                direction: Axis.horizontal,
              ),
            ),
            (AppStrings.dollar + text2)
                .text
                .size(18)
                .fontWeight(FontWeight.w600)
                .color(AppColors.buttonColorBlue)
                .make()
          ],
        ),
      ],
    ).pOnly(left: 15,right: 15);
  }
}
