import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/my_addresses_controllers.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';

class MyAddressesUi extends StatelessWidget {
  const MyAddressesUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyAddressesController controller = Get.find();
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
          elevation: 2,
          leading: InkButton(
            backGroundColor: AppColors.accent,
            child: Icon(
              Icons.arrow_back,
              color: AppColors.white,
            ),
            onTap: () {
              Get.back();
            },
          ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
          title: "My Addresses".text.size(17).color(AppColors.grey).make(),
          centerTitle: true,
          backgroundColor: AppColors.white,
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.search,
                color: Colors.grey,
                size: 25,
              ),
              onPressed: () {},
            )
          ]),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              height: 60,
              width: Get.width,
              decoration: const BoxDecoration(
                  // "vnm"
                  color: Colors.white),
              child: Row(
                children: [
                  "+".text.color(AppColors.blue).make(),
                  "Add a new addresses".text.color(AppColors.blue).make()
                ],
              ).pOnly(left: 10, right: 10).onTap(() {
                Get.toNamed(AppRoutes.editAddress);
              }),
            ),
            "5 saved addresses"
                .text
                .color(AppColors.textGrey)
                .make()
                .pOnly(top: 20, left: 10, right: 10),
            ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: 6,
                itemBuilder: (BuildContext context, int index) {
                  return myAddresses().pOnly(top: 8);
                }),
          ],
        ),
      ),
    ));
  }

  Widget myAddresses() {
    return Material(
      elevation: 10,
      child: Container(
        alignment: Alignment.centerLeft,
        color: AppColors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

          Align(
            alignment: Alignment.topRight,
            child:
          Container(
            alignment: Alignment.center,
            height: 20,
            width: 40,
            child:
            PopupMenuButton(
              iconSize: 15,
              itemBuilder: (BuildContext context) {
                return [
                  PopupMenuItem(
                    child: Text('Remove Address'),
                    value: 'Option 1',
                  ),
                  PopupMenuItem(
                    child: Text('Edit Address'),
                    value: 'Option 2',
                  ),

                ];
              },
              onSelected: (value) {
// print('Selected $value for item ${items[index]}');
              },
              offset: Offset(0, 50), // adjust the position of the menu
            ),
          )
          ),
            Row(
              children: [
                "Name"
                    .text
                    .lineHeight(2)
                    .size(18)
                    .fontWeight(FontWeight.w400)
                    .color(AppColors.textGrey)
                    .make(),
                Container(
                        alignment: Alignment.center,
                        height: 20,
                        width: 45,
                        decoration: BoxDecoration(
                            color: AppColors.buttonColorGrey,
                            borderRadius: BorderRadius.circular(3)),
                        child: "work".text.color(AppColors.white).make())
                    .pOnly(left: 10)
              ],
            ),
            "berkeley Square, Phase 1 industrial area Near By HDFC BANK"
                .text
                .size(14)
                .fontWeight(FontWeight.w400)
                .color(AppColors.textGrey)
                .lineHeight(1.3)
                .make(),
            "Chandigarh, Chandigarh 160002"
                .text
                .size(14)
                .fontWeight(FontWeight.w400)
                .color(AppColors.textGrey)
                .lineHeight(1.8)
                .make(),
            "list.phoneNumber"
                .text
                .size(14)
                .fontWeight(FontWeight.w400)
                .color(AppColors.textGrey)
                .make()
          ],
        ).pOnly(left: 10, right: 10, top: 10),
      ).pOnly(top: 10, bottom: 10),
    );
  }
}
