// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class DebitCardUi extends StatelessWidget {
  const DebitCardUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: AppColors.white,
          leading: InkButton(
              height: 5,
              width: 5,
              borderRadius: 30,
              backGroundColor: AppColors.accent,
              child: Icon(
                Icons.arrow_back,
                color: AppColors.white,
              ),
              onTap: () {
                Get.toNamed(AppRoutes.cart);
              }).pOnly(left: 10, top: 10, bottom: 10, right: 10),
          title: "Top Up"
              .text
              .size(14)
              .fontWeight(FontWeight.w700)
              .color(AppColors.grey)
              .make(),
        ),
        body: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          "Add a debit card"
              .text
              .fontWeight(FontWeight.w500)
              .size(20)
              .color(AppColors.textGrey)
              .make(),
          TextFormField(
            validator: (value) {},
            decoration: InputDecoration(
                filled: true,
                fillColor: AppColors.backgroundColor,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                hintText: "Account name",
                hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: AppColors.textGrey)),
          ).pOnly(top: 30),
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            validator: (value) {},
            decoration: InputDecoration(
              filled: true,
              fillColor: AppColors.backgroundColor,
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(30.0),
              ),
              hintText: "Bank",
              hintStyle: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: AppColors.textGrey),
              suffixIcon: IconButton(
                icon: Icon(
                  Icons.arrow_drop_down_circle,
                  color: AppColors.buttonColorBlue,
                ),
                onPressed: () {},
              ),
            ),
          ).pOnly(top: 15),
          TextField(
            keyboardType: TextInputType.phone,
            autofocus: false,
            style:
                TextStyle(fontWeight: FontWeight.normal, color: AppColors.grey),
            decoration: InputDecoration(
                filled: true,
                fillColor: AppColors.backgroundColor,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                hintText: "Account number",
                hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: AppColors.textGrey)),
          ).pOnly(top: 15).pOnly(top: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 72,
                width: Get.width * .4,
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  validator: (value) {},
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.backgroundColor,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: "Expire",
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: AppColors.textGrey)),
                ).pOnly(top: 15),
              ),
              SizedBox(
                  height: 72,
                  width: Get.width * .4,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    obscureText: true,
                    validator: (value) {},
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: AppColors.backgroundColor,
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        hintText: "Security code",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.textGrey)),
                  ).pOnly(top: 15)),
            ],
          ).pOnly(top: 30),
          InkButton(
                  backGroundColor: AppColors.grey,
                  child: "ADD DEBIT CARD"
                      .text
                      .size(14)
                      .color(AppColors.white)
                      .fontWeight(FontWeight.w700)
                      .make(),
                  onTap: () {})
              .pOnly(top: 200)
        ]).pOnly(left: 10, right: 10)));
  }
}
