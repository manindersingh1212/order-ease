import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/set_password_controller.dart';
import '../utils/localisations/app_colors.dart';

class NewPasswordUi extends StatelessWidget {
  const NewPasswordUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NewPasswordController controller = Get.find();

    return Scaffold(
        body: SingleChildScrollView(
            child: Form(
      key: controller.formkey,
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: 256,
            width: Get.width,
            decoration: BoxDecoration(
                color: AppColors.accent,
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(25),
                    bottomLeft: Radius.circular(25))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 50,
                ),

                InkWell(
                onTap: (){
                  controller.onBackArrow(context);
                },
                child:  Icon(
                  Icons.arrow_back,
                  color: AppColors.white,
                ) ,
              ),
                "Set your password"
                    .text
                    .size(30)
                    .color(AppColors.white)
                    .make()
                    .pOnly(top: 20),
                SizedBox(
                  width: Get.width * .8,
                  child:
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis."
                          .text
                          .size(12)
                          .color(AppColors.white)
                          .lineHeight(2)
                          .make()
                          .pOnly(right: 0),
                ),

              ],
            ),
          ),
          TextFormField(
            controller: controller.passwordController,
            validator: (value) {
              return controller.passwordValidator();
            },
            decoration: InputDecoration(
                filled: true,
                fillColor: AppColors.backgroundColor,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                hintText: "New password",
                hintStyle: TextStyle(color: AppColors.textGrey)),
          ).pOnly(top: 74, left: 20, right: 20),
          TextFormField(
            controller: controller.confirmPasswordController,
            validator: (value) {
              return controller.confirmPasswordValidator();
            },
            decoration: InputDecoration(
                filled: true,
                fillColor: AppColors.backgroundColor,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                hintText: "Confirm password",
                hintStyle: TextStyle(color: AppColors.textGrey)),
          ).pOnly(top: 15, left: 20, right: 20),
          Container(
            alignment: Alignment.center,
            height: 62,
            width: Get.width * .9,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: AppColors.fadedBlue),
            child: "SAVE PASSWORD".text.color(AppColors.white).size(14).make(),
          ).pOnly(top: 170).onTap(() {
            controller.onSaveButton();
          }),
        ],
      ),
    )));
  }
}
