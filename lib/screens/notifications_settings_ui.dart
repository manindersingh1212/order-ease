import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/notifications_settings_controller.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:velocity_x/velocity_x.dart';

class NotificationsSettingsUi extends StatefulWidget {
  const NotificationsSettingsUi({Key? key}) : super(key: key);

  @override
  State<NotificationsSettingsUi> createState() =>
      _NotificationsSettingsUiState();
}

class _NotificationsSettingsUiState extends State<NotificationsSettingsUi> {
  @override
  Widget build(BuildContext context) {
    NotificationsSettingsController controller = Get.find();
    return Scaffold(
      appBar: AppBar(
        elevation: 2,
        leading: InkButton(
          backGroundColor: AppColors.accent,
          child: Icon(
            Icons.arrow_back,
            color: AppColors.white,
          ),
          onTap: () {
            Get.back();
          },
        ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
        title: "Notifications".text.size(17).color(AppColors.grey).make(),
        centerTitle: true,
        backgroundColor: AppColors.white,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            height: 40,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 1,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white, border: Border.all(color: Colors.black12)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(
                      left: 10,
                    ),
                    child: Text("Advertisement notification"),
                  ),
                  Switch(
                      activeTrackColor: Colors.blue.shade50,
                      activeColor: Colors.blue,
                      value: controller.advertisementStatus,
                      onChanged: (value) {
                        setState(() {
                          controller.toggaleSwitches(
                            permission: NotificationPermissions.advertisement,
                          );
                        });
                      })
                ]),
          ),
          const SizedBox(
            height: 40,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 1,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white, border: Border.all(color: Colors.black12)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(
                      left: 10,
                    ),
                    child: Text(" Security alerts notification"),
                  ),
                  Switch(
                      activeTrackColor: Colors.blue.shade50,
                      activeColor: Colors.blue,
                      value: controller.alertStatus,
                      onChanged: (value) {
                        setState(() {
                          controller.toggaleSwitches(
                            permission: NotificationPermissions.alert,
                          );
                        });
                      })
                ]),
          ),
          const SizedBox(
            height: 40,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 1,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white, border: Border.all(color: Colors.black12)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(
                      left: 10,
                    ),
                    child: Text("Message notification"),
                  ),
                  Switch(
                      activeTrackColor: Colors.blue.shade50,
                      activeColor: Colors.blue,
                      value: controller.messageStatus,
                      onChanged: (value) {
                        setState(() {
                          controller.toggaleSwitches(
                              permission: NotificationPermissions.messages);
                        });
                      })
                ]),
          ),
          const SizedBox(
            height: 40,
          ),
          InkButton(
            borderRadius: 10,
            height: 40,
            width: Get.width * .8,
            backGroundColor: controller.colorsOfButton(),
            child: Text(
              controller.buttonName,
              style: TextStyle(color: AppColors.white, fontSize: 20),
            ),
            onTap: () {
              setState(() {
                controller.toggaleSwitches(
                    permission: NotificationPermissions.all);
              });
            },
          ),
        ],
      ),
    );
  }
}
