import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/edit_profile_controller.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:velocity_x/velocity_x.dart';

class EditProfileUi extends StatelessWidget {
  const EditProfileUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    EditProfileController controller = Get.find();

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: InkButton(
            backGroundColor: AppColors.accent,
            child: Icon(
              Icons.arrow_back,
              color: AppColors.white,
            ),
            onTap: () {
              Get.back();
            },
          ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
          backgroundColor: AppColors.white,
          centerTitle: true,
          elevation: 2,
          title: "Edit Profile".text.size(17).color(AppColors.grey).make(),
        ),
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  color: AppColors.accent,
                  height: Get.height * 0.25,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 80,
                        width: 80,
                        decoration: BoxDecoration(
                            color: Colors.green.shade200,
                            border: Border.all(width: 3, color: Colors.white),
                            borderRadius: BorderRadius.circular(45)),
                        child: Image.asset(
                          "assets/images/ic_boy.png",
                          height: 35,
                          width: 35,
                        ),
                      ),
                      "OR"
                          .text
                          .size(13)
                          .color(Colors.white)
                          .make()
                          .paddingOnly(left: 5, right: 5),
                      Container(
                        height: 80,
                        width: 80,
                        decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            border: Border.all(width: 3, color: Colors.white),
                            borderRadius: BorderRadius.circular(45)),
                        child: Image.asset(
                          "assets/images/ic_girl.png",
                          height: 40,
                          width: 40,
                        ),
                      ),
                    ],
                  )),
              Form(
                key: controller.formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: controller.nameController,
                      validator: (v) {
                        return controller.nameValidator();
                      },
                      decoration: const InputDecoration(
                        labelText: "First name",
                      ),
                    ).pOnly(left: 10, right: 10, top: 10),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: "Last Name",
                      ),
                    ).pOnly(left: 10, right: 10, top: 10),
                    InkButton(
                        height: 45,
                        width: Get.width * .3,
                        backGroundColor: AppColors.buttonColorBlue,
                        child: "Submit"
                            .text
                            .size(18)
                            .fontWeight(FontWeight.w500)
                            .color(AppColors.white)
                            .make(),
                        onTap: () {
                          if (controller.formKey.currentState!.validate()) {}
                        }).pOnly(top: 30).centered(),
                    // "SUBMIT".text.color(Colors.blue.shade700).size(18).fontWeight(FontWeight.w500).make().centered().pOnly(top: 40),
                    TextFormField(
                      controller: controller.phoneController,
                      validator: (v) {
                        return controller.phoneValidator();
                      },
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          isDense: true,
                          labelText: "Mobile Number",
                          suffix: const Text("update").onTap(() {
                            if (controller.formKey.currentState!.validate()) {
                            } else {}
                          }),
                          suffixStyle: const TextStyle(
                            fontSize: 16,
                            color: Colors.blue,
                          )),
                    ).pOnly(left: 10, right: 10, top: 10),
                    TextFormField(
                      controller: controller.emailController,
                      validator: (v) {
                        return controller.emailValidator();
                      },
                      decoration: const InputDecoration(
                          labelText: "Email ID",
                          suffixText: "update",
                          suffixStyle: TextStyle(
                            fontSize: 16,
                            color: Colors.blue,
                          )),
                    ).pOnly(left: 10, right: 10, top: 10)
                  ],
                ).pOnly(left: 10, right: 10, top: 10),
              ),
              Container(
                height: 0.3,
                width: Get.width,
                color: Colors.grey.shade500,
              ).pOnly(top: 50),
              "Deactivate Account"
                  .text
                  .size(16)
                  .fontWeight(FontWeight.w400)
                  .make()
                  .pOnly(left: 10, top: 10),
            ],
          ).pOnly(bottom: 20),
        ),
      ),
    );
  }
}
