import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/image_paths.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

class ProfileUI extends StatelessWidget {
  const ProfileUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.white,
            body: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  SizedBox(
                    height: 335,
                    width: Get.width,
                    child: Stack(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: 290,
                          width: Get.width,
                          decoration:
                              BoxDecoration(color: AppColors.backgroundColor),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                backgroundColor: AppColors.white,
                                radius: 50,
                                backgroundImage:
                                const NetworkImage("https://wallpapers.com/images/hd/cute-girl-vector-art-profile-picture-jhbu3wt713zj2bti.jpg"),
                              ),
                              "Kylie"
                                  .text
                                  .fontWeight(FontWeight.w700)
                                  .size(18)
                                  .make(),
                              "Kylie_04@gmail.com"
                                  .text
                                  .fontWeight(FontWeight.w400)
                                  .color(AppColors.grey)
                                  .size(12)
                                  .make(),
                            ],
                          ),
                        ),
                        profileDetails(
                                icons: Image.asset(
                                  ImagesPaths.ic_profile,
                                  color: AppColors.buttonColorBlue,
                                  height: 20,
                                  width: 20,
                                ),
                                title: 'Profile',
                                onTapped: () {
                                  Get.toNamed(AppRoutes.editProfile);
                                })
                            .positioned(top: 256, left: 26, right: 26),
                      ],
                    ),
                  ),
                  profileDetails(
                          icons: Image.asset(
                            ImagesPaths.ic_location,
                            color: AppColors.buttonColorBlue,
                            height: 20,
                            width: 20,
                          ),
                          title: 'My Address',
                          onTapped: () {
                            Get.toNamed(AppRoutes.myAddresses);
                          })
                      .pOnly(
                    left: 26,
                    right: 26,
                  ),
                  profileDetails(
                          icons: Image.asset(
                            ImagesPaths.ic_orderList,
                            color: AppColors.buttonColorBlue,
                            height: 20,
                            width: 20,
                          ),
                          title: 'My Order',
                          onTapped: () {
                            Get.toNamed(AppRoutes.myOrder);
                          })
                      .pOnly(left: 26, right: 26, top: 11),
                  profileDetails(
                          icons: Image.asset(
                            ImagesPaths.ic_love,
                            color: AppColors.buttonColorBlue,
                            height: 20,
                            width: 20,
                          ),
                          title: 'My Favourites',
                          onTapped: () {
                            Get.toNamed(AppRoutes.myFavourites);
                          })
                      .pOnly(left: 26, right: 26, top: 11),
                  profileDetails(
                          icons: Image.asset(
                            ImagesPaths.ic_card,
                            color: AppColors.buttonColorBlue,
                            height: 20,
                            width: 20,
                          ),
                          title: 'Payment',
                          onTapped: () {})
                      .pOnly(left: 26, right: 26, top: 11),
                  profileDetails(
                          icons: Image.asset(
                            ImagesPaths.ic_settings,
                            color: AppColors.buttonColorBlue,
                            height: 20,
                            width: 20,
                          ),
                          title: 'Settings',
                          onTapped: () {
                            Get.toNamed(AppRoutes.settings);
                          })
                      .pOnly(left: 26, right: 26, top: 11),
                ],
              ).pOnly(bottom: 40),
            )));
  }

  Widget profileDetails(
      {required Image icons, required String title, required Function onTapped}) {
    return Container(
        alignment: Alignment.center,
        height: 65,
        width: Get.width * .9,
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: AppColors.grey,
              blurRadius: 4.0,
            ),
          ],
        ),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.,
          children: [
            icons.pOnly(left: 28, right: 22),
            title.text.fontWeight(FontWeight.w700).size(14).color(AppColors.buttonColorBlue).make(),
          ],
        )).onTap(() { 
          onTapped();
    });
  }
}
