// ignore_for_file: must_be_immutable

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/furniture_page_controller.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/app_strings.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class FurnitureUI extends StatefulWidget {
  var data = Get.arguments;
   FurnitureUI({Key? key,}) : super(key: key);
  @override
  State<FurnitureUI> createState() => _FurnitureUIState();
}

class _FurnitureUIState extends State<FurnitureUI> {
  @override
  Widget build(BuildContext context) {
    FurnitureController controller = Get.find();
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColors.white,
        leading: InkButton(
            height: 5,
            width: 5,
            borderRadius: 30,
            backGroundColor: AppColors.accent,
            child: Icon(
              Icons.arrow_back,
              color: AppColors.white,
            ),
            onTap: () {
              Get.toNamed(AppRoutes.mainHome);
            }).pOnly(left: 10, top: 10, bottom: 10, right: 10),
        title: "${widget.data??"Items"}".text.size(14).color(AppColors.grey).make(),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AnimatedContainer(
              duration: const Duration(milliseconds: 500),
              height: controller.height,
              width: Get.width,
              curve: Curves.easeIn,
              decoration: BoxDecoration(
                color: AppColors.backgroundColor,
              ),
              child: GridView.builder(
                  shrinkWrap: true,
                  itemCount: controller.categoryList.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 1,
                    mainAxisSpacing: 29,
                  ),
                  itemBuilder: (_, int index) {
                    return category(
                      controller:  controller,
                      model: controller.categoryList[index],
                    );
                  }),
            ),
            Align(
              alignment: Alignment.topRight,
              child: InkButton(
                  backGroundColor: AppColors.grey,
                  height: 30,
                  width: 80,
                  child:
                  controller.height != 289
                      ? "View All".text.color(AppColors.white).make()
                      :
                  "Hide".text.color(AppColors.white).make(),
                  onTap: () {
                    setState(() {});
                   controller.height==289?controller.hide():controller.onViewAll();
                  }).pOnly(right: 20,top: 20),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: "Popular"
                  .text
                  .size(20)
                  .fontWeight(FontWeight.w500)
                  .color(AppColors.grey)
                  .make(),
            ).pOnly(left: 20, bottom: 20,top: 20),
            ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: controller.popularList.length,
                itemBuilder: (BuildContext context, int index) {
                  return popularWidget(
                      list: controller.popularList[index],
                      index: index,
                      controller: controller);
                }).pOnly(
              left: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget category({
    required CategoryModel model,
    required FurnitureController controller
  }) {
    return
      Column(
      children: [
        SizedBox(
          height: 59,
          width: Get.width * .3,
          child: CachedNetworkImage(
            imageUrl: model.itemsImages,
            placeholder: (context, url) => SizedBox(
              child: LinearProgressIndicator(
                color: AppColors.accentRipple,
                minHeight: 10,
              ),
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
        model.itemsNames.text
            .size(18)
            .fontWeight(FontWeight.w700)
            .color(AppColors.fadedBlue)
            .make(),
        model.numberOfItemsNames.text
            .size(12)
            .fontWeight(FontWeight.w700)
            .color(AppColors.fadedBlue)
            .make(),
      ],
    ).pOnly(top: 10).onTap(() {

      });
  }

  Widget popularWidget(
      {required int index,
      required PopularModel list,
      required FurnitureController controller}) {
    return
      SizedBox(
      height: 160,
      width: Get.width * .9,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            height: 102,
            width: Get.width * .7,
            decoration: BoxDecoration(
                color: AppColors.white,
                boxShadow: [
                  BoxShadow(
                    color: AppColors.grey,
                    blurRadius: 1.0,
                  ),
                ],
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    list.itemsName.text.make(),
                    StatefulBuilder(
                      builder: (BuildContext context,
                          void Function(void Function()) setState) {
                        return InkWell(
                          onTap: () {
                            setState(() {});
                            list.isFav = !list.isFav;
                          },
                          child: Icon(
                            list.isFav ? Icons.favorite : Icons.favorite_border,
                            color: AppColors.red,
                          ),
                        );
                      },
                    ),
                  ],
                ).pOnly(left: 50, right: 15),
                Align(
                  alignment: Alignment.topLeft,
                  child:("${AppStrings.dollar}${list.finalPrice}").text
                      .size(18)
                      .fontWeight(FontWeight.w700)
                      .color(AppColors.buttonColorBlue)
                      .make(),
                ).pOnly(left: 60),
                StatefulBuilder(
                  builder: (BuildContext context,
                      void Function(void Function()) setState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        VxRating(
                          size: 15,
                          normalColor: Colors.yellow.shade200,
                          selectionColor: AppColors.yellow,
                          isSelectable: false,
                          onRatingUpdate: (value) {
                            setState(() {});
                            value = list.rating as String;
                          },
                          value: 2,
                        ),
                        list.rating.text.size(10).color(AppColors.grey).make(),
                        Container(
                          height: 34,
                          width: 61,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: AppColors.accent),
                          child: Icon(
                            Icons.shopping_cart_outlined,
                            color: AppColors.white,
                          ),
                        )
                      ],
                    );
                  },
                ).pOnly(left: 50, right: 20),
              ],
            ),
          ).positioned(left: 70, top: 15),
          Container(
            alignment: Alignment.center,
            height: 132,
            width: 105,
            decoration: BoxDecoration(
                color: AppColors.backgroundColor,
                borderRadius: BorderRadius.circular(15)),
            child:
            CachedNetworkImage(
              imageUrl: list.itemsImages,
              placeholder: (context, url) => SizedBox(
                child: LinearProgressIndicator(
                  color: AppColors.accentRipple,
                  minHeight: 120,
                ),
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
        ],
      ),
    ).onTap(() {
      controller.onPopularItems(index);
      });
  }
}
