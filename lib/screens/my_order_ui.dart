import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/my_order_controller.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';

class MyOrderUI extends StatelessWidget {
  const MyOrderUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyOrderController controller = Get.find();
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        elevation: 2,
        leading: InkButton(
          backGroundColor: AppColors.accent,
          child: Icon(
            Icons.arrow_back,
            color: AppColors.white,
          ),
          onTap: () {
            Get.back();
          },
        ).pOnly(left: 10, top: 10, bottom: 10, right: 10),
        title: "My Orders".text.size(17).color(AppColors.grey).make(),
        centerTitle: true,
        backgroundColor: AppColors.white,
        actions: <Widget>[
          InkButton(
            height: 40,
            width: 40,
            rippleColor: AppColors.accentRipple,
            backGroundColor: Colors.grey.shade100,
            onTap: () {
              Get.toNamed(AppRoutes.cart);
            },
            child: Icon(
              Icons.shopping_cart,
              size: 20,
              color: AppColors.grey,
            ),
          ).pOnly(right: 25)
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: controller.orderList.length,
                itemBuilder: (BuildContext context, int index) {
                  return orderWidget(model: controller.orderList[index]);
                }).pOnly(top: 20)
          ],
        ).pOnly(left: 10, right: 10),
      ),
    ));
  }

  Widget orderWidget({required MyOrderDetailsModel model}) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              height: 70,
              width: Get.width * .25,
              child: CachedNetworkImage(
                fit: BoxFit.fitHeight,
                imageUrl: model.itemImage,
                placeholder: (context, url) => SizedBox(
                  child: LinearProgressIndicator(
                    color: AppColors.accentRipple,
                    minHeight: 120,
                  ),
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            SizedBox(
              height: 70,
              width: Get.width * .6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  model.date.text.make(),
                  const Align(
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.navigate_next,
                        size: 15,
                      )),
                  model.itemName.text.make()
                ],
              ),
            ),
          ],
        ),
        const Divider()
      ],
    );
  }
}
