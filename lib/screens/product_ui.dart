import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:page_view_indicators/circle_page_indicator.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/product_controller.dart';
import '../utils/localisations/app_colors.dart';

class ProductUi extends StatefulWidget {
  const ProductUi({Key? key}) : super(key: key);

  @override
  State<ProductUi> createState() => _ProductUiState();
}

class _ProductUiState extends State<ProductUi> {
  final _currentPageNotifier = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    ProductPageController controller = Get.put(ProductPageController());

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        leading:InkButton(

          backGroundColor: AppColors.accent, child: Icon(Icons.arrow_back,color: AppColors.white,),
          onTap: (){
            Get.back();
          },
        ).pOnly(left: 10, top: 10, bottom: 10, right: 10),

        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: SlidingUpPanel(
        minHeight: 290,
        maxHeight: 500,
        panel: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                controller.product.name.text
                    .size(18)
                    .fontWeight(FontWeight.w500)
                    .color(AppColors.grey)
                    .make()
                    .pOnly(left: 0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: Get.width * 0.25,
                      child: RatingBarIndicator(
                        rating: 2,
                        itemBuilder: (context, index) => Icon(
                          Icons.star,
                          color: AppColors.yellow,
                        ),
                        itemCount: 5,
                        itemSize: 20.0,
                        direction: Axis.horizontal,
                      ),
                    ),
                    "\$${controller.product.price}"
                        .text
                        .size(30)
                        .fontWeight(FontWeight.w600)
                        .color(AppColors.grey)
                        .make(),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    "Size".text.size(12).fontWeight(FontWeight.w400).make(),
                    SizedBox(
                      height: 30,
                      width: Get.width * .5,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: controller.itemsSizes.length,
                          itemBuilder: (_, int index) {
                            return itemsSizes(
                                index: index,
                                sizesList: controller.itemsSizes,
                                controller: controller);
                          }),
                    )
                  ],
                ).pOnly(top: 27),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    "Choose a color :"
                        .text
                        .size(12)
                        .fontWeight(FontWeight.w400)
                        .make(),
                    SizedBox(
                      height: 30,
                      width: Get.width * .5,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: controller.itemsColors.length,
                          itemBuilder: (_, int index) {
                            return itemsColors(
                                index: index,
                                colorList: controller.itemsColors,
                                controller: controller);
                          }),
                    )
                  ],
                ).pOnly(top: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    " Select Quality :"
                        .text
                        .size(12)
                        .fontWeight(FontWeight.w400)
                        .make(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: AppColors.backgroundColor),
                          child: "-"
                              .text
                              .size(20)
                              .fontWeight(FontWeight.w700)
                              .color(AppColors.textGrey)
                              .make(),
                        ).onTap(() {
                          controller.decrementItems(setState);
                        }),
                        Obx(
                          () => Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColors.backgroundColor),
                            child: controller.numberOfItems.value.text.make(),
                          ).pOnly(left: 10),
                        ),
                        Container(
                          alignment: Alignment.center,
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: AppColors.backgroundColor),
                          child: "+"
                              .text
                              .size(20)
                              .fontWeight(FontWeight.w600)
                              .color(AppColors.textGrey)
                              .make(),
                        ).pOnly(left: 10).onTap(() {
                          controller.incrementItems(setState);
                        }),
                      ],
                    ).pOnly(top: 16)
                  ],
                ),
                "Description"
                    .text
                    .size(12)
                    .color(AppColors.grey)
                    .fontWeight(FontWeight.w400)
                    .make(),
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan\nerat id facilisis. Dues acute irure in reprehenderit in voluptate velit."
                    .text
                    .color(AppColors.grey)
                    .make(),
              ],
            ),
            InkButton(
              backGroundColor: AppColors.buttonColorBlue,
              height: 62,
              child: "ADD TO CART"
                  .text
                  .size(14)
                  .color(AppColors.white)
                  .fontWeight(FontWeight.w700)
                  .make(),
              onTap: () {
                controller.continueButton();
              },
            ).pOnly(top: 19),
          ],
        ).pOnly(left: 30, right: 30, top: 40, bottom: 16)),
        body: Column(children: [
          Container(
            width: Get.width,
            color: Colors.white,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              viewPagerWithIndicator(controller),
              Container(
                height: 1,
                width: Get.width,
                color: Colors.grey.shade200,
              ).pOnly(top: 0, bottom: 10),
            ]),
          ),
        ]),
      ),
    ));
  }

  Widget indicator(ProductPageController controllers) {
    return SmoothPageIndicator(
      controller: controllers.controller,
      count: 6,
      axisDirection: Axis.horizontal,
      effect: const SlideEffect(
          spacing: 8.0,
          radius: 10.0,
          dotWidth: 8.0,
          dotHeight: 8.0,
          paintStyle: PaintingStyle.stroke,
          strokeWidth: 1.5,
          dotColor: Colors.grey,
          activeDotColor: Colors.red),
    );
  }

  viewPagerWithIndicator(ProductPageController controller) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              alignment: Alignment.center,
              child: SizedBox(
                height: 405,
                child: PageView.builder(
                    itemCount: 3,
                    controller: controller.controller,
                    itemBuilder: (BuildContext context, int index) {
                      return viewPagerBanners(index, controller);
                    },
                    onPageChanged: (int index) {
                      _currentPageNotifier.value = index;
                    }),
              ),
            ),
            Positioned(
              left: 0.0,
              right: 0.0,
              bottom: 0.0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CirclePageIndicator(
                  itemCount: 3,
                  currentPageNotifier: _currentPageNotifier,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget viewPagerBanners(int index, ProductPageController controller) {
    return Container(
      width: Get.width,
      height: 190,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(controller.product.productImg, scale: 1

                // "assets/images/banner1.jpg",
                )),
        borderRadius: BorderRadius.circular(10),
      ),
    ).px(5);
  }

  Widget itemsSizes(
      {required int index,
      required List sizesList,
      required ProductPageController controller}) {
    return Container(
      alignment: Alignment.center,
      height: 30,
      width: 30,
      decoration: BoxDecoration(
          color: controller.selectedSize == index
              ? AppColors.buttonColorBlue
              : AppColors.grey,
          borderRadius: BorderRadius.circular(8)),
      child: FittedBox(
        child: "${sizesList[index]}"
            .text
            .size(12)
            .color(AppColors.white)
            .fontWeight(FontWeight.w700)
            .make(),
      ),
    ).pOnly(left: 3).onTap(() {
      setState(() {
        controller.selectedSize = index;
      });
    });
  }

  Widget itemsColors(
      {required int index,
      required List colorList,
      required ProductPageController controller}) {
    return Container(
      alignment: Alignment.center,
      height: 24,
      width: 30,
      decoration: BoxDecoration(
          border: Border.all(
              color: controller.selectedColor == index
                  ? AppColors.accentRipple
                  : AppColors.white,
              width: 3),
          color: colorList[index],
          borderRadius: BorderRadius.circular(15)),
    ).pOnly(left: 3).onTap(() {
      setState(() {
        controller.selectedColor = index;
      });
    });
  }
}
