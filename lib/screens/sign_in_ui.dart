import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/sign_in_controller.dart';
import '../utils/localisations/app_colors.dart';
import '../utils/localisations/image_paths.dart';
import '../utils/routes/app_routes.dart';

class SignInUI extends StatelessWidget {
  const SignInUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SignInController controller = Get.find();
    return SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.white,
            body: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(children: [
                Container(
                  alignment: Alignment.center,
                  height: 220,
                  width: Get.width,
                  decoration: BoxDecoration(
                      color: AppColors.accent,
                      borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(25),
                          bottomLeft: Radius.circular(25))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                             InkButton(
                            backGroundColor: AppColors.accent,
                            height:40,
                              width: 80,
                              child:"SIGN UP"
                                    .text
                                    .size(14)
                                    .color(AppColors.white)
                                    .make(),
                              onTap: (){
                            Get.toNamed(AppRoutes.signUp);
                          }),
                        ],
                      ).pOnly(right: 31, top: 30),
                      "Sign in"
                          .text
                          .size(30)
                          .color(AppColors.white)
                          .make()
                          .pOnly(top: 10, bottom: 10),
                      SizedBox(
                          width: Get.width * .8,
                          child:
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis."
                                  .text
                                  .size(12)
                                  .color(AppColors.white)
                                  .lineHeight(1.5)
                                  .make())
                    ],
                  ).pOnly(left: 20),
                ),
                Form(
                    key: controller.formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: controller.emailController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            return controller.emailValidator();
                          },
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: AppColors.backgroundColor,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              hintText: "Enter your Email",
                              hintStyle: TextStyle(color: AppColors.textGrey)),
                        ).pOnly(bottom: 40),
                        StatefulBuilder(
                          builder: (BuildContext context,
                              void Function(void Function()) setState) {
                            return TextFormField(
                              validator: (value) {
                                return controller.passwordValidator();
                              },
                              controller: controller.passwordController,
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: controller.isPasswordVisible.value,
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: AppColors.backgroundColor,
                                  suffixIcon:
                                  InkButton(
                                      backGroundColor: AppColors.backgroundColor,
                                      rippleColor: AppColors.accentRipple,
                                      height:40,
                                      width: 50,
                                      child: controller.isPasswordVisible.value
                                              ? Image.asset(ImagesPaths.ic_eye,height: 25,width: 25,)
                                              : Icon(
                                                  Icons.remove_red_eye,
                                                  color: AppColors.grey,
                                                ),
                                      onTap: (){
                                        setState((){});
                              controller.showPassword();
                                      }).pOnly(right: 10),

                                  border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  hintText: "Password",
                                  hintStyle:
                                      TextStyle(color: AppColors.textGrey)),
                            );
                          },
                        ).pOnly(bottom: 20)
                      ],
                    )).pOnly(left: 12, right: 21, top: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    "Forgot password?"
                        .text
                        .size(12)
                        .color(AppColors.red)
                        .make()
                        .pOnly(right: 31),
                  ],
                ).onTap(() {
                  controller.onForgotPassword(context);
                }),
                InkButton(
                  height: 52,
                  backGroundColor: AppColors.buttonColorBlue,
                  rippleColor: AppColors.accentRipple,
                  child: "SIGN IN".text.color(AppColors.white).size(14).make(),
                  onTap: (){
                    controller.signInButtonValidation();

                  },
                ).pOnly(top: 30),
                "Or Sign in with social media"
                    .text
                    .size(12)
                    .fontWeight(FontWeight.w400)
                    .lineHeight(5)
                    .make()
                    .pOnly(bottom: 25),
                InkButton(
                  onTap: () {},
                  rippleColor: AppColors.accentRipple,
                  backGroundColor: AppColors.backgroundColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset(ImagesPaths.ic_googleLogo),
                      "CONTINUE WITH GOOGLE"
                          .text
                          .size(13)
                          .color(AppColors.textGrey)
                          .fontWeight(FontWeight.w700)
                          .make(),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 38,
                ),
                InkButton(
                        onTap: () {
                          controller.loginToFacebook();
                        },
                        backGroundColor: AppColors.blue,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image.asset(ImagesPaths.ic_facebookLogo),
                            "CONTINUE WITH FACEBOOK"
                                .text
                                .size(12)
                                .color(AppColors.white)
                                .fontWeight(FontWeight.w700)
                                .make(),
                          ],
                        ),
                        rippleColor: AppColors.accentRipple)
                    .marginOnly(bottom: 40),
              ]),
            )));
  }
}
