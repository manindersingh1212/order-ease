import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:like_button/like_button.dart';
import 'package:order_ease_e_com/controllers/home_controller.dart';
import 'package:order_ease_e_com/controllers/product_controller.dart';
import 'package:order_ease_e_com/utils/localisations/app_strings.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/localisations/image_paths.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';

class HomeUI extends StatelessWidget {
  const HomeUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.white,
            body: SingleChildScrollView(
                child: Column(
              children: [
                SizedBox(
                  height: 335,
                  width: Get.width,
                  child: Stack(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: 300,
                        width: Get.width,
                        decoration: BoxDecoration(
                            color: AppColors.backgroundColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 45,
                                  width: Get.width * .7,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        height: 45,
                                        width: Get.width * .5,
                                        child: TextFormField(
                                          autofocus: false,
                                          onTap: () {
                                            Get.toNamed(AppRoutes.searchPage);
                                          },
                                          readOnly: true,
                                          decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "Search",
                                              hintStyle: TextStyle(
                                                  color: AppColors.grey)),
                                        ),
                                      ).pOnly(left: 10),
                                      InkButton(
                                          height: 45,
                                          width: 60,
                                          backGroundColor: AppColors.accent,
                                          child: Icon(
                                            Icons.search,
                                            color: AppColors.white,
                                          ),
                                          onTap: () {
                                            Get.toNamed(AppRoutes.searchPage);
                                          }),
                                    ],
                                  ),
                                ),
                                InkButton(
                                    height: 45,
                                    width: Get.width * .12,
                                    backGroundColor: AppColors.accent,
                                    child: Icon(
                                      Icons.shopping_cart_outlined,
                                      color: AppColors.white,
                                    ),
                                    onTap: () {
                                      Get.toNamed(AppRoutes.cart);
                                    })
                              ],
                            ).pOnly(left: 20, right: 20, bottom: 30),
                            Align(
                              alignment: Alignment.topLeft,
                              child: "Category"
                                  .text
                                  .size(20)
                                  .fontWeight(FontWeight.w500)
                                  .color(AppColors.grey)
                                  .make(),
                            ).pOnly(left: 20, bottom: 10),
                            SizedBox(
                              height: 108,
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  itemCount: controller.categoryList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return categoryWidget(
                                        list: controller.categoryList[index]);
                                  }),
                            )
                          ],
                        ).pOnly(
                          top: 20,
                        ),
                      ),
                      Container(
                        height: 65,
                        width: Get.width * .8,
                        decoration: BoxDecoration(
                          color: AppColors.orange,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.account_balance_wallet_outlined,
                                  color: AppColors.white,
                                  size: 30,
                                ),
                                SizedBox(
                                    width: 30,
                                    child: FittedBox(
                                        child: "\$${controller.walletBalance}"
                                            .text
                                            .color(AppColors.white)
                                            .bold
                                            .make())),
                              ],
                            ),
                            Image.asset(
                              ImagesPaths.ic_pay,
                              height: 40,
                              width: 40,
                            ),
                            Image.asset(
                              ImagesPaths.ic_top_up,
                              height: 40,
                              width: 40,
                            ).onTap(() {
                              Get.toNamed(AppRoutes.topUp);
                            }),
                          ],
                        ),
                      ).positioned(top: 266, left: 26, right: 26),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: "Sale Discount"
                      .text
                      .size(20)
                      .fontWeight(FontWeight.w500)
                      .color(AppColors.grey)
                      .make(),
                ).pOnly(
                  left: 20,
                  bottom: 24,
                  top: 25,
                ),
                SizedBox(
                  height: 200,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: controller.saleDiscountList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return saleDiscountWidget(
                          controller: controller,
                          list: controller.saleDiscountList[index],
                          discountImage: ImagesPaths.ic_discount,
                          index: index,
                        );
                      }),
                ).pOnly(
                  left: 20,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: "Popular"
                      .text
                      .size(20)
                      .fontWeight(FontWeight.w500)
                      .color(AppColors.grey)
                      .make(),
                ).pOnly(left: 20, bottom: 20),
                ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.popularList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return popularWidget(
                          list: controller.popularList[index],
                          index: index,
                          controller: controller);
                    }).pOnly(
                  left: 20,
                ),
              ],
            ).pOnly(bottom: 40))));
  }

  Widget categoryWidget({required CategoryModel list}) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8),
      child: InkButton(
          height: 22,
          width: 79,
          backGroundColor: AppColors.white,
          borderRadius: 10,
          child: Column(
            children: [
              Image.asset(
                list.catIcon,
                height: 32,
              ),
              SizedBox(
                width: 64,
                child: list.catName.text
                    .size(12)
                    .align(TextAlign.center)
                    .color(AppColors.buttonColorBlue)
                    .fontWeight(FontWeight.w700)
                    .make()
                    .pOnly(left: 10, top: 5),
              )
            ],
          ).pOnly(top: 15),
          onTap: () {
            Get.toNamed(AppRoutes.furniture, arguments: list.catName);
          }),
    );
  }

  Widget saleDiscountWidget(
      {required int index,
      required discountImage,
      required SaleDiscountModel list,
      required HomeController controller}) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Stack(
        children: [
          Container(
            height: 180,
            width: 122,
            decoration: BoxDecoration(
              color: AppColors.backgroundColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CachedNetworkImage(
                  height: 120,
                  width: Get.width,
                  imageUrl: list.itemImage,
                  placeholder: (context, url) => SizedBox(
                    child: LinearProgressIndicator(
                      color: AppColors.accentRipple,
                      minHeight: 10,
                    ),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    list.itemsName.text
                        .color(AppColors.grey)
                        .size(12)
                        .fontWeight(FontWeight.w500)
                        .make(),
                    Row(
                      children: [
                        ("${AppStrings.dollar}${list.finalPrice}")
                            .text
                            .size(14)
                            .fontWeight(FontWeight.w700)
                            .color(AppColors.buttonColorBlue)
                            .make(),
                        const SizedBox(
                          width: 20,
                        ),
                        (AppStrings.dollar + list.MRP)
                            .text
                            .fontWeight(FontWeight.w600)
                            .textStyle(const TextStyle(
                                decoration: TextDecoration.lineThrough))
                            .size(8)
                            .color(AppColors.grey)
                            .make(),
                      ],
                    )
                  ],
                ).pOnly(left: 15, bottom: 10)
              ],
            ),
          ),
          Image.asset(discountImage, height: 52, width: 40)
              .positioned(bottom: 150, left: 70),
          "Disc\n${list.discount}"
              .text
              .size(12)
              .color(AppColors.white)
              .make()
              .positioned(
                left: 75,
              )
        ],
      ),
    ).onTap(() {
      Get.toNamed(AppRoutes.productPage,
          arguments: ProductDetails(
              controller.saleDiscountList[index].itemsName,
              controller.saleDiscountList[index].finalPrice,
              controller.saleDiscountList[index].itemImage));
    });
  }

  Widget popularWidget(
      {required int index,
      required PopularModel list,
      required HomeController controller}) {
    return SizedBox(
      height: 160,
      width: Get.width * .9,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            height: 102,
            width: Get.width * .7,
            decoration: BoxDecoration(
                color: AppColors.white,
                boxShadow: [
                  BoxShadow(
                    color: AppColors.grey,
                    blurRadius: 1.0,
                  ),
                ],
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    list.itemsName.text.make(),
                    StatefulBuilder(
                      builder: (BuildContext context,
                          void Function(void Function()) setState) {
                        return InkWell(
                          onTap: () {
                            setState(() {});
                          },
                          child:
                          const LikeButton(
                            bubblesColor: BubblesColor(
                                dotPrimaryColor: Color.fromARGB(255, 85, 255, 7),
                                dotSecondaryColor: Color(0xFFACFD3E),
                                dotThirdColor: Color(0xFFFF5722),
                                dotLastColor: Color(0xFFF44336)),
                            size: 28,
                          ),
                        );
                      },
                    ),
                  ],
                ).pOnly(left: 50, right: 15),
                Align(
                  alignment: Alignment.topLeft,
                  child: ("${AppStrings.dollar}${list.finalPrice}")
                      .text
                      .size(18)
                      .fontWeight(FontWeight.w700)
                      .color(AppColors.buttonColorBlue)
                      .make(),
                ).pOnly(left: 60),
                StatefulBuilder(
                  builder: (BuildContext context,
                      void Function(void Function()) setState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        VxRating(
                          size: 15,
                          normalColor: Colors.yellow.shade200,
                          selectionColor: AppColors.yellow,
                          isSelectable: false,
                          onRatingUpdate: (value) {
                            setState(() {});
                            value = list.rating as String;
                          },
                          value: 2,
                        ),
                        list.rating.text.size(10).color(AppColors.grey).make(),
                        Container(
                          height: 34,
                          width: 61,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: AppColors.accent),
                          child: Icon(
                            Icons.shopping_cart_outlined,
                            color: AppColors.white,
                          ),
                        )
                      ],
                    );
                  },
                ).pOnly(left: 50, right: 20),
              ],
            ),
          ).positioned(left: 70, top: 15),
          Container(
              alignment: Alignment.center,
              height: 132,
              width: 105,
              decoration: BoxDecoration(
                  color: AppColors.backgroundColor,
                  borderRadius: BorderRadius.circular(15)),
              child: CachedNetworkImage(
                imageUrl: list.itemsImages,
                placeholder: (context, url) => SizedBox(
                  child: LinearProgressIndicator(
                    color: AppColors.accentRipple,
                    minHeight: 10,
                  ),
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              )
              // Image.network(list.itemsImages),
              ),
        ],
      ),
    ).onTap(() {
      Get.toNamed(AppRoutes.productPage,
          arguments: ProductDetails(
            controller.popularList[index].itemsName,
            controller.popularList[index].finalPrice,
            controller.popularList[index].itemsImages,
          ));
    });
  }
}
