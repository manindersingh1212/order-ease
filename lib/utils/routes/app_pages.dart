import 'package:get/get.dart';
import 'package:order_ease_e_com/bindings/furniture_page_binding.dart';
import 'package:order_ease_e_com/bindings/main_home_binding.dart';
import 'package:order_ease_e_com/bindings/verify_code_binding.dart';
import 'package:order_ease_e_com/screens/choose_language_ui.dart';
import 'package:order_ease_e_com/screens/debit_card_Ui.dart';
import 'package:order_ease_e_com/screens/furniture_page_ui.dart';
import 'package:order_ease_e_com/screens/my_addresses_ui.dart';
import 'package:order_ease_e_com/screens/payments_ui.dart';
import 'package:order_ease_e_com/screens/top_up_Ui.dart';
import 'package:order_ease_e_com/screens/verify_code_Ui.dart';

import '../../bindings/cart_binding.dart';
import '../../bindings/choose_language_binding.dart';
import '../../bindings/debit_card_bindings.dart';
import '../../bindings/edit_address_binding.dart';
import '../../bindings/edit_profile_binding.dart';
import '../../bindings/forgot_password_binding.dart';
import '../../bindings/forgot_password_method_binding.dart';
import '../../bindings/home_binding.dart';
import '../../bindings/introduction_binding.dart';
import '../../bindings/my_addresses_binding.dart';
import '../../bindings/my_favourites_binding.dart';
import '../../bindings/my_order_binding.dart';
import '../../bindings/notfication_bindings.dart';
import '../../bindings/notifications_settings_binding.dart';
import '../../bindings/payments_binding.dart';
import '../../bindings/product_binding.dart';
import '../../bindings/profile_binding.dart';
import '../../bindings/search_page_bindings.dart';
import '../../bindings/set_password_binding.dart';
import '../../bindings/settings_binding.dart';
import '../../bindings/sign_in_binding.dart';
import '../../bindings/sign_up_binding.dart';
import '../../bindings/splash_binding.dart';
import '../../bindings/top_up_bindings.dart';
import '../../screens/cart_ui.dart';
import '../../screens/edit_address_ui.dart';
import '../../screens/edit_profile_ui.dart';
import '../../screens/forgot_password_method_ui.dart';
import '../../screens/forgot_password_ui.dart';
import '../../screens/home_ui.dart';
import '../../screens/introduction_ui.dart';
import '../../screens/main_home_ui.dart';
import '../../screens/my_favourites_ui.dart';
import '../../screens/my_order_ui.dart';
import '../../screens/notfication_ui.dart';
import '../../screens/notifications_settings_ui.dart';
import '../../screens/product_ui.dart';
import '../../screens/profile_ui.dart';
import '../../screens/search_page_ui.dart';
import '../../screens/set_password_ui.dart';
import '../../screens/settings_ui.dart';
import '../../screens/sign_in_ui.dart';
import '../../screens/sign_up_ui.dart';
import '../../screens/splash_ui.dart';
import 'app_routes.dart';

class AppPages {
  static const initialRoute = AppRoutes.splash;

  static final route = [
    GetPage(
        name: AppRoutes.splash,
        page: () => const SplashUI(),
        binding: SplashBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.introduction,
        page: () => const IntroductionUi(),
        binding: IntroductionBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.signIn,
        page: () => const SignInUI(),
        binding: SignInBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.signUp,
        page: () => const SignUpUI(),
        binding: SignUpBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.forgotPasswordMethod,
        page: () => const ForgotMethodUI(),
        binding: ForgotMethodBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.forgotPassword,
        page: () => const ForgotPasswordUI(),
        binding: ForgotPasswordBinding()),
    GetPage(
        name: AppRoutes.setPassword,
        page: () => const NewPasswordUi(),
        binding: NewPasswordBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.home,
        page: () => const HomeUI(),
        binding: HomeBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.profile,
        page: () => const ProfileUI(),
        binding: ProfileBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.mainHome,
        page: () => const MainHomeUI(),
        binding: MainHomeBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.cart,
        page: () => const CartUI(),
        binding: CartBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.furniture,
        page: () => FurnitureUI(),
        binding: FurnitureBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.searchPage,
        page: () => const SearchPageUi(),
        binding: SearchPageBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.productPage,
        page: () => const ProductUi(),
        binding: ProductPageBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.payments,
        page: () => PaymentsUi(),
        binding: PaymentBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.topUp,
        page: () => TopUpUi(),
        binding: TopUpBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.debitCard,
        page: () => const DebitCardUi(),
        binding: DebitCardBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.verifyCode,
        page: () => const VerifyCodeUi(),
        binding: VerifyBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.myFavourites,
        page: () => const MyFavouritesUI(),
        binding: MyFavouritesBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.myOrder,
        page: () => const MyOrderUI(),
        binding: MyOrderBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.editAddress,
        page: () => const EditAddressUI(),
        binding: EditAddressBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.myAddresses,
        page: () => const MyAddressesUi(),
        binding: MyAddressesBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.notification,
        page: () => const NotificationUi(),
        binding: NotificationBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.editProfile,
        page: () => const EditProfileUi(),
        binding: EditProfileBinding()),
    GetPage(
        name: AppRoutes.notification,
        page: () => const NotificationUi(),
        binding: NotificationBinding()),
    GetPage(
        name: AppRoutes.chooseLanguage,
        page: () => const ChooseLanguageUi(),
        binding: ChooseLanguageBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.settings,
        page: () => const SettingsUI(),
        binding: SettingsBinding(),
        transition: Transition.native),
    GetPage(
        name: AppRoutes.notificationsSettings,
        page: () => const NotificationsSettingsUi(),
        binding: NotificationsSettingsBinding(),
        transition: Transition.native),
  ];
}
