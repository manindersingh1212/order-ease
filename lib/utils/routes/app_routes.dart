class AppRoutes {
  static const String splash = "/splash";
  static const String introduction = "/introduction";

  static const String signIn = "/signIn";
  static const String signUp = "/signUp";
  static const String forgotPasswordMethod = "/forgotPasswordMethod";
  static const String forgotPassword = "/forgotPassword";
  static const String setPassword = "/setPassword";
  static const String home = "/home";
  static const String profile = "/profile";
  static const String mainHome = "/mainHome";
  static const String searchPage = "/searchPage";
  static const String productPage = "/productPage";
  static const String payments = "/payments";



  static const String cart = "/cart";
  static const String furniture = "/furniture";
  static const String paymentMethod = "/paymentMethod";
  static const String topUp = "/topUp";
  static const String debitCard = "/debitCard";
  static const String verifyCode = "/verifyCode";

  static const String myFavourites = "/myFavourites";
  static const String myOrder = "/myOrder";
  static const String editAddress = "/editAddress";

  static const String notification = "/notification";
  static const String myAddresses = "/myAddresses";

  static const String editProfile = "/editProfile";

  static const String chooseLanguage = "/chooseLanguage";
  static const String settings = "/settings";
  static const String notificationsSettings = "/notificationsSettings";
}
// "/**/"