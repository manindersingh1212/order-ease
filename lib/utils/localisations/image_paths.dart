// ignore_for_file: constant_identifier_names
class ImagesPaths {

  //lottiePaths

static const String splash="assets/lottie_animations/splash_lottie.json";
//imagesPaths
static const String ic_eye="assets/images/ic_eye.png";
static const String ic_facebookLogo="assets/images/ic_facebook_logo.png";
static const String ic_googleLogo="assets/images/ic_google_logo.png";
static const String ic_card="assets/images/ic_card.png";
static const String ic_location="assets/images/ic_location.png";
static const String ic_love="assets/images/ic_love.png";
static const String ic_orderList="assets/images/ic_order_list.png";
static const String ic_profile="assets/images/ic_profile.png";
static const String ic_profile1="assets/images/ic_profile1.png";
static const String ic_settings="assets/images/ic_settings.png";
static const String ic_child_clothes="assets/images/ic_child_clothes.png";
static const String ic_foodAndDrinks="assets/images/ic_foodAndDrinks.png";
static const String ic_furniture="assets/images/ic_furniture.png";
static const String ic_handphone="assets/images/ic_handphone.png";
static const String ic_guitar="assets/images/ic_guitar.png";
static const String ic_kitchen_tools="assets/images/ic_kitchen_tools.png";
static const String ic_shoes="assets/images/ic_shoes.png";
static const String ic_pay="assets/images/ic_pay.png";
static const String ic_more="assets/images/ic_more.png";
static const String ic_my_pay="assets/images/ic_my_pay.png";
static const String ic_top_up="assets/images/ic_top_up.png";
static const String ic_discount="assets/images/ic_discount.png";
static const String ic_home="assets/images/ic_home.png";
static const String ic_notification="assets/images/ic_notification.png";
static const String ic_coin="assets/images/ic_coin.png";
}

