import 'package:get/get.dart';

import '../controllers/payments_controllers.dart';

class PaymentBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(PaymentsController());
  }
}