import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/settings_controller.dart';

class SettingsBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(SettingsController());
  }
}