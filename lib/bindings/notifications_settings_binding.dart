import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/notifications_settings_controller.dart';

class NotificationsSettingsBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(NotificationsSettingsController());
  }
}