import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/choose_language_controller.dart';

class ChooseLanguageBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(ChooseLanguageController());
  }
}