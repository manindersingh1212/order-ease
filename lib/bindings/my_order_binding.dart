import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/my_order_controller.dart';

class MyOrderBinding implements Bindings{
  @override
  void dependencies() {
 Get.put(MyOrderController() );
  }
}