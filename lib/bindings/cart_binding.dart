import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/cart_controller.dart';

class CartBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(CartController());
  }
}