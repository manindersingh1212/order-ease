import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/my_favourites_controller.dart';

class MyFavouritesBinding implements Bindings{
  @override
  void dependencies() {
Get.put(MyFavouritesController());  }
}