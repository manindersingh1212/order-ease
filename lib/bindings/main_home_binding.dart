import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/home_controller.dart';

import '../controllers/main_home_controller.dart';
import '../controllers/notfication_controller.dart';

class MainHomeBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(MainHomeController());
        Get.put(HomeController());
        Get.put(NotificationController());
  }
}