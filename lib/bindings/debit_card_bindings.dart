import 'package:get/get.dart';

import '../controllers/debit_card_controller.dart';

class DebitCardBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(DebitCardController());
  }
}
