import 'package:get/get.dart';

import '../controllers/product_controller.dart';

class ProductPageBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(ProductPageController());
  }
}