import 'package:get/get.dart';

import '../controllers/my_addresses_controllers.dart';

class MyAddressesBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(MyAddressesController());
  }
}