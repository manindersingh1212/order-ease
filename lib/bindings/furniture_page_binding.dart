import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/furniture_page_controller.dart';

class FurnitureBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(FurnitureController());
  }
}
