import 'package:get/get.dart';

import '../controllers/verfiy_code_Controller.dart';

class VerifyBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(VerifyController());
  }
}