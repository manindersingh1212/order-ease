import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/edit_address_controller.dart';

class EditAddressBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(EditAddressController());
  }
}