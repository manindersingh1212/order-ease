import 'package:get/get.dart';

import '../controllers/forgot_password_method_controller.dart';
import '../controllers/sign_up_controller.dart';

class SignUpBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(SignUpController());
    Get.put(ForgotMethodController());
  }
}