import 'package:get/get.dart';

import '../controllers/notfication_controller.dart';

class NotificationBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(NotificationController());
  }
}