import 'package:get/get.dart';

import '../controllers/forgot_password_method_controller.dart';
import '../controllers/sign_up_controller.dart';

class ForgotMethodBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(ForgotMethodController());
    Get.put(SignUpController());
  }
}