import 'package:get/get.dart';

import '../controllers/top_up_controller.dart';

class TopUpBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(TopUpController());
  }
}