import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';
import '../utils/localisations/custom_widgets.dart';
import 'forgot_password_controller.dart';


class VerifyController extends SuperController{

  final formkey = GlobalKey<FormState>();
  var firstFocusNode = FocusNode();
  final secondFocusNode = FocusNode();
  final thirdFocusNode = FocusNode();
  final fourthFocusNode = FocusNode();
  TextEditingController firstFieldController = TextEditingController();
  TextEditingController secondFieldController = TextEditingController();
  TextEditingController thirdFieldController = TextEditingController();
  TextEditingController fourthFieldController = TextEditingController();

  // ignore: body_might_complete_normally_nullable
  void deleteOTP({required FieldType fieldType}) {
    if (fieldType == FieldType.first) {
      if (firstFieldController.text.isNotEmpty) {
        firstFocusNode.unfocus();
        secondFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.second) {
      if (secondFieldController.text.isNotEmpty) {
        secondFocusNode.unfocus();
        thirdFocusNode.requestFocus();
      } else {
        secondFocusNode.unfocus();
        firstFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.third) {
      if (thirdFieldController.text.isNotEmpty) {
        thirdFocusNode.unfocus();
        fourthFocusNode.requestFocus();
      } else {
        thirdFocusNode.unfocus();
        secondFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.fourth) {
      if (fourthFieldController.text.isNotEmpty) {
        fourthFocusNode.unfocus();
      } else {
        fourthFocusNode.unfocus();
        thirdFocusNode.requestFocus();
      }
    }
  }

  void onConfirmButtonSubmitted(context) {
    if (firstFieldController.text == "" ||
        secondFieldController.text == "" ||
        thirdFieldController.text == "" ||
        fourthFieldController.text == "") {
      snackBar(title: "OrderEase", message: "Please fill all the fields");
    } else if (firstFieldController.text +
        secondFieldController.text +
        thirdFieldController.text +
        fourthFieldController.text !=
        "1234") {
      snackBar(title: "OrderEase", message: "Invalid Pin");
    } else {
      showModalBottomSheet<void>(
        backgroundColor: AppColors.white,
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: 460,
            child: Column(
                crossAxisAlignment:
                CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 5,
                    width: Get.width * .1,
                    decoration: BoxDecoration(
                        color: AppColors.grey,
                        borderRadius:
                        BorderRadius
                            .circular(5)),
                  ).pOnly(top: 12),
                  Container(
                    alignment: Alignment.center,
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                        color: AppColors.yellow,
                        borderRadius:
                        BorderRadius
                            .circular(40)),
                    child: Icon(
                      Icons.done,
                      color: AppColors.white,
                      size: 40,
                    ),
                  ).pOnly(top: 35),
                  "SUCCESS"
                      .text
                      .size(18)
                      .fontWeight(
                      FontWeight.w700)
                      .color(AppColors.grey)
                      .make()
                      .pOnly(
                      top: 25, bottom: 40),
                  "Thank you for purchasing. Your order will be "
                      .text
                      .size(12)
                      .fontWeight(
                      FontWeight.w400)
                      .lineHeight(2)
                      .color(AppColors.grey)
                      .make(),
                  "shipped 2 - 4 working days."
                      .text
                      .size(12)
                      .fontWeight(
                      FontWeight.w400)
                      .color(AppColors.grey)
                      .make(),
                  Container(
                    alignment: Alignment.center,
                    height: 60,
                    width: Get.width * .9,
                    decoration: BoxDecoration(
                      color: AppColors
                          .buttonColorBlue,
                      borderRadius:
                      BorderRadius.circular(
                          50),
                    ),
                    child: "CONTINUE SHOPPING"
                        .text
                        .size(14)
                        .fontWeight(
                        FontWeight.w700)
                        .color(AppColors.white)
                        .make(),
                  )
                      .pOnly(
                    top: 30,
                  )
                      .onTap(() {})
                ]).pOnly(
                bottom: 20,
                left: 30,
                right: 30),
          );
        },
      );


    }
  }
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}