
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:velocity_x/velocity_x.dart';
import '../utils/routes/app_routes.dart';

class SignInController extends SuperController {
  ForgotMethods? methods;
  AccessToken? accessToken;
  RxBool checking = true.obs;
  final formKey = GlobalKey<FormState>();
  RxBool isPasswordVisible = true.obs;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  void chooseForgotMethods({required ForgotMethods type}) {
    methods = type;
    Get.toNamed(AppRoutes.forgotPasswordMethod);
  }
  Future<void> loginToFacebook() async {
    final LoginResult result = await FacebookAuth.instance.login();
    if (result.status == LoginStatus.success) {
      final getStorage=GetStorage();
      getStorage.write("accessToken", "loggedIn");
      Get.offAllNamed(AppRoutes.mainHome);
      accessToken = result.accessToken;
      var userData = await FacebookAuth.instance.getUserData();
      userData = userData;
    } else {
      snackBar(title: "OrderEase",message:"Failed to login with facebook" );

  }

  }
  void onForgotPassword(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(16.0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              'Forgot Password'.text.size(14).bold.make(),
              InkButton(
                rippleColor: AppColors.accentRipple,
                  backGroundColor: AppColors.backgroundColor,
                  height: 23,
                  width: 20,
                  child: const Icon(CupertinoIcons.multiply_circle), onTap: (){
                Navigator.of(context).pop();
              }),
            ],
          ),
          content: SizedBox(
            height: 80,
            width: Get.width,
            child:
            Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InkButton(
                            rippleColor: AppColors.accentRipple,

                            backGroundColor: AppColors.accent,
                            height: 40,
                            width: Get.width*.3,


                            child:"Via mail".text.fontWeight(FontWeight.w400).color(AppColors.white).make(),
                            onTap: (){
                              Navigator.of(context).pop();
                              chooseForgotMethods(type: ForgotMethods.viaMail);
                            },

                          ),
                          InkButton(
                            rippleColor: AppColors.accentRipple,
                            backGroundColor: AppColors.accent,

                            height: 40,
                            width: Get.width*.3,


                              child:"Via phone".text.fontWeight(FontWeight.w400).color(AppColors.white).make(),
                            onTap: (){
                              Navigator.of(context).pop();
                              chooseForgotMethods(type: ForgotMethods.viaPhone);
                            },

                          ),
                        ],
                      ),
          ),
        );
      },
    );
  }

  void showPassword() {
    isPasswordVisible.value = !isPasswordVisible.value;
    printData("$isPasswordVisible");
  }

  String? emailValidator() {
    if (EmailValidator.validate(emailController.text)) {
      return null;
    } else {
      return "Invalid Email";
    }
  }

  String? passwordValidator() {
    if (passwordController.text.length < 8) {
      return "Password must be 8 digits";
    }
    return null;
  }

  void signInButtonValidation() {
    if (formKey.currentState!.validate()) {
      final token = GetStorage();
      token.write("accessToken", "loggedIn").whenComplete(() {
        printData("Get Token");
      });
      Get.offAllNamed(AppRoutes.mainHome);
    } else {}
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}

enum ForgotMethods { viaMail, viaPhone }
