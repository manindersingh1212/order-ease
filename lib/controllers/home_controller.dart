// ignore_for_file: non_constant_identifier_names


import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/image_paths.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';

class HomeController extends SuperController{
  num walletBalance=12;
  String updateListKey = "updateListKey";
  List<CategoryModel> categoryList=[];
  List<SaleDiscountModel> saleDiscountList=[];
  List<PopularModel> popularList=[];
  RxBool showMoreOptions=true.obs;
  @override
  void onInit() {
    categoryList.add(CategoryModel(ImagesPaths.ic_shoes, "Women Fashion"));
    categoryList.add(CategoryModel(ImagesPaths.ic_child_clothes, "Men Fashion"));
    categoryList.add(CategoryModel(ImagesPaths.ic_child_clothes, "Child Fashion"));
    categoryList.add(CategoryModel(ImagesPaths.ic_foodAndDrinks, "Food & Drinks"));
    categoryList.add(CategoryModel(ImagesPaths.ic_kitchen_tools, "Kitchen Tools"));
    categoryList.add(CategoryModel(ImagesPaths.ic_furniture, "Furniture"));
    categoryList.add(CategoryModel(ImagesPaths.ic_guitar, "Hobby"));
    categoryList.add(CategoryModel(ImagesPaths.ic_handphone, "Electronics"));
    
    saleDiscountList.add(SaleDiscountModel("Camelia Heels", 64, "50%", "125","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2ZlBCdF6yVgdN8yEC54JTz_6BFbQrMkxPXA&usqp=CAU"));
    saleDiscountList.add(SaleDiscountModel("Converse Shoes", 89, "40%", "135","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLK-A0iMMPFd3fi_3xrAYtmsxhfBkGriJ4eg&usqp=CAU"));
    saleDiscountList.add(SaleDiscountModel("Levi's Jacket", 94, "40%", "135","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTN66qPrebUIBJmEU-O8TMJbsED-NGB2NP5Jw&usqp=CAU"));
    saleDiscountList.add(SaleDiscountModel("Aphira Chair", 147, "20%", "185","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-AnS3he4m0czrJw0lULvL2Jm4iD7ZNK5HIA&usqp=CAU"));
    saleDiscountList.add(SaleDiscountModel("Pan", 65, "20%", "85","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSX6aGYcuDwp4Wbkyndojf3nv7O37BrmUvMsg&usqp=CAU"));

    popularList.add(PopularModel("Vinta Backpack", 78,"https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",2.4,false));
    popularList.add(PopularModel("Kimono Clogs", 64,"https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",3.4,true));
    super.onInit();
  }
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }

  void onLikeTap(int index) {

    popularList[index].isFav=!popularList[index].isFav;
    printData("${popularList[index].isFav}");
    update([updateListKey]);
  }
}
class CategoryModel {
  String catName="";
  String catIcon="";
  CategoryModel(this.catIcon,this.catName);
}
class SaleDiscountModel {
  String itemImage="";
  String itemsName="";
  int finalPrice;
  String discount="";
  String MRP="";
  SaleDiscountModel(this.itemsName,this.finalPrice,this.discount,this.MRP,this.itemImage);
}
class PopularModel {
  String itemsName="";
  int finalPrice;
  String itemsImages="";
  double rating=0.0;
  bool isFav=false;
  PopularModel(this.itemsName,this.finalPrice,this.itemsImages,this.rating,this.isFav);
}