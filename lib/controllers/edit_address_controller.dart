import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';

import '../utils/routes/app_routes.dart';

class EditAddressController extends SuperController{
  var formKey=GlobalKey<FormState>();
  FocusNode nameFocusNode = FocusNode();
  FocusNode phoneFocusNode = FocusNode();
  FocusNode pinFocusNode = FocusNode();
  FocusNode loctionFocusNode = FocusNode();
  FocusNode stateFocusNode = FocusNode();
  FocusNode cityFocusNode = FocusNode();
  FocusNode houseFousNode = FocusNode();
  FocusNode areaFocusNode = FocusNode();
  FocusNode buttonFocusNode = FocusNode();
  TextEditingController nameController =TextEditingController();
  TextEditingController phoneController =TextEditingController();
  TextEditingController alternativePhoneNumberController =TextEditingController();
  TextEditingController pinCodeController =TextEditingController();
  TextEditingController stateController =TextEditingController();
  TextEditingController cityController =TextEditingController();
  TextEditingController houseNumberController =TextEditingController();
  TextEditingController roadNameController =TextEditingController();
  RxBool showAlternativePhoneNumber=false.obs;
  List<AddressModel>editedAddressList=[];
  String? nameValidator(){
    if(nameController.text.length<2){
      return "Name should be atleast 2 characters";
    }
    return null;
  }
  String? phoneValidator(){
    if(phoneController.text.length<10){
      return "Invalid phone number";
    }
    return null;
  }  String? alternativePhoneNumberValidator(){
    if(alternativePhoneNumberController.text.length<10){
      return "Invalid phone number";
    }
    return null;
  }

  String? houseNumberValidator(){
    if(houseNumberController.text.isEmpty){
      return "Please fill this field";
    }
    return null;
  }
  String? cityNameValidator(){
    if(cityController.text.isEmpty){
      return "Please fill this field";
    }
    return null;
  }
  String? roadNameValidator(){
    if(roadNameController.text.isEmpty){
      return "Please fill this field";
    }
    return null;
  }
  String? stateValidator(){
    if(stateController.text.isEmpty){
      return "Please fill this field";
    }
    return null;
  }
  String? pinCodeValidator(){
    if(pinCodeController.text.length<6){
      return "Invalid PinCode";
    }
    return null;
  }
  void onSaveAddress(){
   if(formKey.currentState!.validate()){
     
     editedAddressList.add(AddressModel(nameController.text, phoneController.text , pinCodeController.text, stateController.text, cityController.text, houseNumberController.text, roadNameController.text));
  Get.offNamed(AppRoutes.myAddresses,arguments: editedAddressList);
     printData("${editedAddressList.length}");

   }else
   {
     printData("NotDone");

   }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
class AddressModel{
  String name="";
  String? phoneNumber;
  String? pinCode;
  String roadName="";
  String cityName="";
  String stateName="";
  String houseNumber="";
  AddressModel(this.name,this.phoneNumber,this.pinCode,this.stateName,this.cityName,this.houseNumber,this.roadName);
}