import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/app_colors.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';

class ProductPageController extends GetxController {
  int initialValue = 0;
// df
  ProductDetails product = Get.arguments;
  int selectedSize = -1;
  int selectedColor = -1;

   List<Color> itemsColors = [
    AppColors.red,
    AppColors.blue,
    AppColors.fadedBlack,
    AppColors.pink,
    AppColors.accent,
    AppColors.red,
    AppColors.blue,
    AppColors.fadedBlack,
    AppColors.pink,
    AppColors.accent,
  ];
  List<String> itemsSizes = [
    "S",
    "M",
    "L",
    "XL",
    "XXL",
    "XXXL",
    "4XL",
    "5XL",
  ];

  RxInt numberOfItems = 1.obs;
  List<String> photo = [
    "assets/images/ic_d3.png",
    "assets/images/ic_d4.png",
    "assets/images/ic_d1.png",
  ];

  PageController controller = PageController();

  void incrementItems(StateSetter setState) {
    numberOfItems.value++;
    product.price = initialValue * numberOfItems.value;
    printData("${product.price}");
    printData("${numberOfItems.value}");
    setState(() {});
  }

  void decrementItems(StateSetter setState) {
    if (numberOfItems.value > 1) {
      numberOfItems.value--;
      product.price = product.price - initialValue;
      printData("${product.price}");
      printData("${numberOfItems.value}");
      setState(() {});
    }
  }

  void continueButton() {
 Get.toNamed(AppRoutes.cart);
  }

  @override
  void onInit() {
    initialValue = product.price;
    super.onInit();
    controller = PageController(
      viewportFraction: 0.8,
    );
  }
}

class ProductDetails {
  String name = "";
  int price;
  double rating = 0.0;
  String productImg = "";

  ProductDetails(
    this.name,
    this.price,
    this.productImg, {
    this.rating = 0.9,
  });
}
