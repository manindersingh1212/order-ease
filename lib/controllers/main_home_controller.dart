import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/screens/home_ui.dart';
import 'package:order_ease_e_com/screens/notfication_ui.dart';
import 'package:order_ease_e_com/screens/profile_ui.dart';

class MainHomeController extends SuperController{
  RxInt selectedIndex=0.obs;
  static const List<Widget> widgetOptions = <Widget>[
    HomeUI(),
    NotificationUi(),
    ProfileUI()
  ];

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}