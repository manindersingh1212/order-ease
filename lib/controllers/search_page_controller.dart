import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class SearchPageController extends SuperController{
  int numberOfItemsFound=20;
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
  RxBool changeButton = false.obs;

  button() {
    changeButton.value = !changeButton.value;
  }
}