
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';


class ChooseLanguageController extends GetxController {
  List<String> languageList = [
    "English",
    "Hindi",
    "Marathi",
    "Punjabi",
    "Tamil",
    "English",
    "Hindi",
    "Marathi",
    "Punjabi",
    "Tamil"
  ];

  RxInt selectedItem=(-1).obs;

  void checkBox(int index) {

    if(selectedItem.value!=index){

      selectedItem.value=index;
    } else{
      selectedItem.value = -1;
    }

  }

  Rx<Color> selectedColor() {
    if(selectedItem.value ==-1 ){
      return Colors.grey.obs;
    } else {
      return Colors.orange.shade900.obs;
    }
  }
  void onContinueButton(){
    if(selectedItem.value==-1){
      const GetSnackBar(title:"Flipkart",message: "Please select one language to continue",duration: Duration(seconds: 2),backgroundColor: Colors.blue,snackPosition: SnackPosition.TOP,).show();
    }else{
      Get.offAllNamed(AppRoutes.settings);

    }
  }
}
