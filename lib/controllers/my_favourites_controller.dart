import 'package:get/get.dart';

class MyFavouritesController extends SuperController {
  List<MyFavouritesItems> favourite = [];

  @override
  void onInit() {
    favourite.add(MyFavouritesItems(
        "Vinta Backpack",
        78,
        "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
        2.4,
        true));
    favourite.add(MyFavouritesItems(
        "Kimono Clogs",
        64,
        "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
        3.4,
        true));

    super.onInit();
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}

class MyFavouritesItems {
  String itemsName = "";
  int finalPrice;
  String itemsImages = "";
  double rating = 0.0;
  bool isFav = false;

  MyFavouritesItems(this.itemsName, this.finalPrice, this.itemsImages,
      this.rating, this.isFav);
}
