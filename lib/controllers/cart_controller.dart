import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';

class CartController extends SuperController {
  List<CartModel> allCartItems = [];
  List<int> selectedCartItems = [];
  int totalPrice = 0;


  void calculateAll(){

  }

  void cart(Types type, CartModel model, index, StateSetter setState) {

    int s = 0;

    if (type == Types.increment) {
      allCartItems[index].numberOfCartItem++;
      // int items = allCartItems[index].numberOfCartItem;
      // printData("Items $items}");
      // printData("Items ${model.subTotalOfItem}");
      allCartItems[index].subTotalOfItem =
          model.price * allCartItems[index].numberOfCartItem;
      //
      // printData("TotalPrice ${allCartItems[index].subTotalOfItem}");
      // printData("Cart Items ${allCartItems[index].numberOfCartItem}");

      setState(() {});
      for (int i = 0; i <= allCartItems.length - 1; i++) {
        if (selectedCartItems.contains(i)) {
          s = s + allCartItems[i].subTotalOfItem;
          // printData("${allCartItems[i].subTotalOfItem}");
          totalPrice=s;

        }
      }

    }
    if (type == Types.decrement) {
      if (allCartItems[index].numberOfCartItem > 1) {
        allCartItems[index].numberOfCartItem--;
        // printData("Items ${model.subTotalOfItem}");
        allCartItems[index].subTotalOfItem =
            model.subTotalOfItem - allCartItems[index].price;

        // printData("TotalPrice ${allCartItems[index].subTotalOfItem}");
        // printData("Cart Items ${allCartItems[index].numberOfCartItem}");
      }

      setState(() {});
      for (int i = 0; i <= allCartItems.length - 1; i++) {
        if (selectedCartItems.contains(i)) {
          s = s + allCartItems[i].subTotalOfItem;
          // printData("${allCartItems[i].subTotalOfItem}");
          totalPrice=s;

        }
      }

    }

    if (type == Types.deleteItemsFromCart) {
      setState(() {
        allCartItems.removeAt(index);
      });
      for (int i = 0; i <= allCartItems.length - 1; i++) {
        if (selectedCartItems.contains(i)) {
          selectedCartItems.removeAt(index);
          allCartItems[i].subTotalOfItem=0;

        }
      }
    }

    if (type == Types.addItems) {
      if (selectedCartItems.contains(index)) {
        selectedCartItems.remove(index);
      } else {
        selectedCartItems.add(index);
      }
      for (int i = 0; i <= allCartItems.length - 1; i++) {
        if (selectedCartItems.contains(i)) {
          s = s + allCartItems[i].subTotalOfItem;
          // printData("${allCartItems[i].subTotalOfItem}");
          totalPrice=s;

        }
      }

    }

    if (type == Types.calculateSubTotalOfSelectedItems) {
      int localTotal = 0;
      for (int i = 0; i <= selectedCartItems.length - 1; i++) {
        localTotal = localTotal + selectedCartItems[i];
        // printData("Index: $i Total Price: $totalPrice");
      }
      totalPrice = localTotal;
    }
    printData("All Total $s");
  }
  @override
  void onInit() {
    allCartItems.add(CartModel(
      "L",
      4,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Levi's Jeans",
      "Dark Grey",
      4,
      1,
    ));
    allCartItems.add(CartModel(
      "No size",
      5,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Milano Parada",
      "Silver",
      5,
      1,
    ));
    allCartItems.add(CartModel(
      "No size",
      5,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Milano Parada",
      "Silver",
      5,
      1,
    ));
    allCartItems.add(CartModel(
      "No size",
      3,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Milano Parada",
      "Silver",
      5,
      1,
    ));
    allCartItems.add(CartModel(
      "No size",
      2,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Milano Parada",
      "Silver",
      5,
      1,
    ));
    allCartItems.add(CartModel(
      "No size",
      5,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Milano Parada",
      "Silver",
      5,
      1,
    ));
    allCartItems.add(CartModel(
      "No size",
      5,
      "https://rlv.zcache.com/emerald_green_and_silver_sparkles_neck_tie-r94b9bbabca40482aad1fdd0b1f22d6c4_z5nrg_492.jpg?rlvnet=1",
      "Milano Parada",
      "Silver",
      5,
      1,
    ));
    super.onInit();
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}

class CartModel {
  int price = 0;
  String itemsName = "";
  String size = "";
  String itemsImage = "";
  String itemsColor = "";
  int subTotalOfItem = 0;
  int numberOfCartItem = 1;

  CartModel(
    this.size,
    this.price,
    this.itemsImage,
    this.itemsName,
    this.itemsColor,
    this.subTotalOfItem,
    this.numberOfCartItem,
  );
}

enum Types {
  increment,
  decrement,
  calculateSubTotalOfSelectedItems,
  deleteItemsFromCart,
  addItems
}
