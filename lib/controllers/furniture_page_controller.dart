import 'package:get/get.dart';
import 'package:order_ease_e_com/controllers/product_controller.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';

class FurnitureController extends SuperController{
  List<CategoryModel>categoryList=[];
  List<PopularModel> popularList=[];

  double height= 150.0;

  @override
  void onInit() {
    popularList.add(PopularModel("Green Chair", 97,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRezXzWeuEM0Or0c-aysq63HtIkckakCM7SDQ&usqp=CAU",2.4,false));
    popularList.add(PopularModel("Spring Sofa", 224,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8yIU8kEUjrgren9JxhYfzUIV-4yM2AWK0Dw&usqp=CAU",2.4,false));
    popularList.add(PopularModel("Annalis", 78,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFti9el0NmmOmxcYpIsSGlW62OAWHxz2t9yQ&usqp=CAU",3.4,true));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8yIU8kEUjrgren9JxhYfzUIV-4yM2AWK0Dw&usqp=CAU", "Sofa", "12 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRezXzWeuEM0Or0c-aysq63HtIkckakCM7SDQ&usqp=CAU", "Chair", "17 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScfyVIw-mvAGPZTw0C5E18BdgDx61RbsP1sw&usqp=CAU", "Bed", "22 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMxrzGMwWJnCT6wU5KAUlVfaYy9Qpcz5URmQ&usqp=CAU", "Drawer", "10 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTamnXCoNUt5m2kUrW36qZSKJJMRpCFh05FTQ&usqp=CAU", "Plant", "14 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfi6FVAqd0uRVthk3oSFAHnnf22CJEzZkrGg&usqp=CAU", "Pillow", "19 items")); categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8yIU8kEUjrgren9JxhYfzUIV-4yM2AWK0Dw&usqp=CAU", "Sofa", "12 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRezXzWeuEM0Or0c-aysq63HtIkckakCM7SDQ&usqp=CAU", "Chair", "17 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScfyVIw-mvAGPZTw0C5E18BdgDx61RbsP1sw&usqp=CAU", "Bed", "22 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMxrzGMwWJnCT6wU5KAUlVfaYy9Qpcz5URmQ&usqp=CAU", "Drawer", "10 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTamnXCoNUt5m2kUrW36qZSKJJMRpCFh05FTQ&usqp=CAU", "Plant", "14 items"));
    categoryList.add(CategoryModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfi6FVAqd0uRVthk3oSFAHnnf22CJEzZkrGg&usqp=CAU", "Pillow", "19 items"));
    super.onInit();
  }
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }

  void onViewAll() {

    height=289;
  }
  void onPopularItems(int index){
    Get.toNamed(AppRoutes.productPage,
      arguments: ProductDetails(popularList[index].itemsName, popularList[index].finalPrice, popularList[index].itemsImages, rating: popularList[index].rating,),
    );
  }
  void hide() {

    height=150;
  }
}
class CategoryModel{
  String itemsImages="";
  String itemsNames="";
  String numberOfItemsNames="";
  CategoryModel(this.itemsImages,this.itemsNames,this.numberOfItemsNames);

}
class PopularModel {
  String itemsName="";
  int finalPrice;
  String itemsImages="";
  double rating=0.0;
  bool isFav=false;
  PopularModel(this.itemsName,this.finalPrice,this.itemsImages,this.rating,this.isFav);
}