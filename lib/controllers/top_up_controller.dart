import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';


class TopUpController extends SuperController{
  TextEditingController textController=TextEditingController();
  RxString tenRupees="10".obs;
  RxString fiftyRupees="50".obs;
  RxString hundredRupees="100".obs;
  RxString twoHundredRupees="200".obs;
 void onSelectDollar({required SelectedDollarValue value}){
   if(value==SelectedDollarValue.ten){
     textController.text=tenRupees.value;
   }
   if(value==SelectedDollarValue.fifty){
     textController.text=fiftyRupees.value;
   }
   if(value==SelectedDollarValue.hundred){
     textController.text=hundredRupees.value;
   }
   if(value==SelectedDollarValue.twoHundred){
     textController.text=twoHundredRupees.value;
   }
 }

 @override

  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
enum SelectedDollarValue{ten,fifty,hundred,twoHundred}