// ignore_for_file: body_might_complete_normally_nullable

import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../utils/routes/app_routes.dart';

class ForgotMethodController extends SuperController {
  OTPMethods? optType;
  final emailFormkey = GlobalKey<FormState>();
  final phoneFormkey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();

  String? emailValidator() {
    if (EmailValidator.validate(emailController.text)) {

    } else {
      return "Invalid email";
    }
  }
  String? phoneValidator() {
    if (phoneController.text.length<10) {
      return "Invalid phone";
    }
  }

  void onEmailContinueButton({required OTPMethods otpMethods}) async{
    if (emailFormkey.currentState!.validate()) {
      optType=otpMethods;
     await Get.toNamed(AppRoutes.forgotPassword);
      emailController.clear();
    }
  }
  void onPhoneContinueButton({required OTPMethods otpMethods})async {
    if (phoneFormkey.currentState!.validate()) {
      optType=otpMethods;
     await Get.toNamed(AppRoutes.forgotPassword);
      phoneController.clear();
    }
  }
 

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
enum OTPMethods{viaMail,viaPhone}
