// ignore_for_file: non_constant_identifier_names, unnecessary_null_comparison

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';
import 'forgot_password_method_controller.dart';

class ForgotPasswordController extends SuperController {
  final formkey = GlobalKey<FormState>();
  var firstFocusNode = FocusNode();
  final secondFocusNode = FocusNode();
  final thirdFocusNode = FocusNode();
  final fourthFocusNode = FocusNode();
  TextEditingController firstFieldController = TextEditingController();
  TextEditingController secondFieldController = TextEditingController();
  TextEditingController thirdFieldController = TextEditingController();
  TextEditingController fourthFieldController = TextEditingController();

  // ignore: body_might_complete_normally_nullable
  void deleteOTP({required FieldType fieldType}) {
    if (fieldType == FieldType.first) {
      if (firstFieldController.text.isNotEmpty) {
        firstFocusNode.unfocus();
        secondFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.second) {
      if (secondFieldController.text.isNotEmpty) {
        secondFocusNode.unfocus();
        thirdFocusNode.requestFocus();
      } else {
        secondFocusNode.unfocus();
        firstFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.third) {
      if (thirdFieldController.text.isNotEmpty) {
        thirdFocusNode.unfocus();
        fourthFocusNode.requestFocus();
      } else {
        thirdFocusNode.unfocus();
        secondFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.fourth) {
      if (fourthFieldController.text.isNotEmpty) {
        fourthFocusNode.unfocus();
      } else {
        fourthFocusNode.unfocus();
        thirdFocusNode.requestFocus();
      }
    }
  }

  void onConfirmButtonSubmitted(ForgotMethodController forgotMethodController) {
    if (firstFieldController.text == "" ||
        secondFieldController.text == "" ||
        thirdFieldController.text == "" ||
        fourthFieldController.text == "") {
      snackBar(title: "OrderEase", message: "Please fill all the fields");
    } else if (firstFieldController.text +
        secondFieldController.text +
        thirdFieldController.text +
        fourthFieldController.text !=
        "1234") {
      snackBar(title: "OrderEase", message: "Invalid OTP");
    } else {
      final token=GetStorage();
      token.write("accessToken", 'loggedIn');
      printData("Get Token");
      forgotMethodController.optType == OTPMethods.viaMail ?

      Get.offAllNamed(AppRoutes.setPassword):forgotMethodController.optType == OTPMethods.viaMail?Get.offAllNamed(AppRoutes.setPassword):Get.offAllNamed(AppRoutes.mainHome);
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}

enum FieldType { first, second, third, fourth }
