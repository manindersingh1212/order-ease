import 'package:get/get.dart';

class IntroductionController extends SuperController{
  List<IntroData> introData = [];


  @override
  void onInit() {
    introData.add(IntroData("SHOPPING FROM HOME","Lorem ipsum dolor sit amet\nconsectetur adipiscing elit.Integer\nmaximus accumsan erat id facilisis.","assets/images/ic_intro1.png",));
    introData.add(IntroData("PRODUK ORIGINAL","Lorem ipsum dolor sit amet\nconsectetur adipiscing elit.\nIntegermaximus accumsan erat id facilisis.","assets/images/ic_intro2.png"));
    introData.add(IntroData("EXPRESS DELIVERY","Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.\nIntegermaximus accumsan erat id facilisis.","assets/images/ic_intro3.png"));
    super.onInit();
  }
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}

class IntroData{
  String title="";
  String body="";
  String imagePath = "";

  IntroData(this.title,this.body,this.imagePath);
}