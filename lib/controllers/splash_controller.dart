import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class SplashController extends SuperController{
@override
  void onInit() {
    Future.delayed(const Duration(seconds: 5),(){
      final storage=GetStorage();
      final token =storage.read("accessToken");
  if(token==""){
    Get.offAllNamed(AppRoutes.signIn);
    printData("splash done");
  }else{
    Get.offAllNamed(AppRoutes.mainHome);
    printData("splash done");
  }
    });
    super.onInit();
  }
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}