

// ignore_for_file: body_might_complete_normally_nullable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:order_ease_e_com/utils/localisations/custom_widgets.dart';
import 'package:order_ease_e_com/utils/routes/app_routes.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/localisations/app_colors.dart';

class NewPasswordController extends SuperController {
  final formkey = GlobalKey<FormState>();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
 void onBackArrow(context){
   Navigator.of(context).pop();
 }
  String? passwordValidator() {
    if (passwordController.text.length < 8) {
      return "Password should be atleast 8 digits";
    }
  }

  String? confirmPasswordValidator() {
    if (passwordController.text != confirmPasswordController.text) {
      return "Password doesn't match";
    }
  }

  void onSaveButton() {
    if (formkey.currentState!.validate()) {
      Get.offAllNamed(AppRoutes.mainHome);
    } else {
      snackBar(title: "OrderEase", message: "Password doesn't match");
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
