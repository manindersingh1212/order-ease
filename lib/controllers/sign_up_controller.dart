// ignore_for_file: body_might_complete_normally_nullable

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../utils/localisations/custom_widgets.dart';
import '../utils/routes/app_routes.dart';

class SignUpController extends SuperController{
  final formKey = GlobalKey<FormState>();
  RxBool isPasswordVisible = true.obs;
  RxBool isConfirmPasswordVisible = true.obs;
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  void showPassword() {
    isPasswordVisible.value = !isPasswordVisible.value;
    printData("$isPasswordVisible");

  }
  void showConfirmPassword() {
    isConfirmPasswordVisible.value = !isConfirmPasswordVisible.value;
    printData("$isConfirmPasswordVisible");
  }

  String? emailValidator() {
    if (EmailValidator.validate(emailController.text)) {
      return null;
    } else {
      return "Invalid email";
      // kjk
    }
  }

String? nameValidator() {
    if (nameController.text.length<3) {
      return "Invalid name";
    }
  }

  String? passwordValidator() {
    if (passwordController.text.length < 8) {
      return "Password must be 8 digits";
    }
  }
  String? confirmPasswordValidator() {
    if (passwordController.text!=confirmPasswordController.text) {
      return "New password does not match";
    }
  }

  void signUpButtonValidation() {
    if (formKey.currentState!.validate()) {

      Get.toNamed(AppRoutes.forgotPassword);
    } else {
      printData("failed");
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}