import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotificationsSettingsController extends SuperController {
  bool advertisementStatus = false;
  bool alertStatus = false;
  bool messageStatus = false;
  String buttonName = "Subscribe all";

  /// Change Notification Permissions
  void toggaleSwitches({
    required NotificationPermissions permission,
  }) {
    ///Message Permission
    if (permission == NotificationPermissions.messages) {
      messageStatus = !messageStatus;
    }

    ///Alert Permission
    else if (permission == NotificationPermissions.alert) {
      alertStatus = !alertStatus;
    }

    ///Advertisement Permission
    else if (permission == NotificationPermissions.advertisement) {
      advertisementStatus = !advertisementStatus;
    }

    /// Subscribe - Unsubscribe all
    else if (permission == NotificationPermissions.all) {
      (advertisementStatus == true &&
              alertStatus == true &&
              messageStatus == true)
          ? unSubscribeAll()
          : subscribeAll();
    }
    subscribeUnsubscribeButton();
  }

  /// Allow all notification permissions
  void unSubscribeAll() {
    advertisementStatus = false;
    alertStatus = false;
    messageStatus = false;
  }

  /// Deny all Notification Permissions
  void subscribeAll() {
    advertisementStatus = true;
    alertStatus = true;
    messageStatus = true;
  }

  /// Change button name
  void subscribeUnsubscribeButton() {
    if (advertisementStatus == true &&
        alertStatus == true &&
        messageStatus == true) {
      buttonName = "Remove all from subscribe";
    } else {
      buttonName = "Subscribe all";
    }
  }

  //Color of button
  colorsOfButton() {
    if (advertisementStatus == true &&
        alertStatus == true &&
        messageStatus == true) {
      return Colors.red;
    } else {
      return Colors.green;
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}

enum NotificationPermissions {
  messages,
  alert,
  advertisement,
  all;
}
