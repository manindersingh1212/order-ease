import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';


class EditProfileController extends SuperController{

  final formKey = GlobalKey<FormState>();



  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  String? nameValidator() {
    if (nameController.text.length > 2) {
      return null;
    }
    return "Name should be atleast 3 characters";
  }
  String? phoneValidator() {
    if (phoneController.text.length > 9) {
      return null;
    }
    return "phone should be atleast 10 characters";
  }
  String? emailValidator() {
    if (EmailValidator.validate(emailController.text)) {
      return null;
    } else {
      return "Invalid email";
    }
  }


  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}